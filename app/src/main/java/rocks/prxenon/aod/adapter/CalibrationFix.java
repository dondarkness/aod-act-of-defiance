package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;
import com.squareup.picasso.Transformation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.settings.ScreenCalibration;


public class CalibrationFix extends BaseAdapter {

    private final Activity context;
    List<String> boxindex, boxvalue;
    private final Integer thesize;





    public CalibrationFix(Activity context, Integer thesize, List<String> boxindex, List<String> boxvalue) {

        // TODO Auto-generated constructor stub

        this.context=context;
        this.thesize = thesize;
        this.boxindex = boxindex;
        this.boxvalue = boxvalue;
    }

    @Override
    public int getCount() {
        return boxvalue.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

         View gridView;

        // this.cache.initializeCache();
        if (convertView == null) {
            Log.i("View -->", " is null +" + position);
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.libs_calibrationfix, null);
            TextView valuex = gridView.findViewById(R.id.turfnamet);


               valuex.setText(boxvalue.get(position));

            int padding = valuex.getPaddingTop();

            valuex.setBackground(context.getResources().getDrawable(R.drawable.buttonstyle));
            valuex.setEnabled(false);
            valuex.setTextColor(context.getResources().getColor(R.color.colorWhite));
            valuex.setPadding(padding, padding, padding, padding);

            //  history_value_sub.setText(""+timeset[position]);
            //


            Log.i("LOG ", boxvalue.get(position));



        } else {
            Log.i("View -->", " is not null +" + position);
            gridView = convertView;
        }

        return gridView;


    }


    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

}