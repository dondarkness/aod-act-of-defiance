package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.AODMain;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class RankingAdapter extends BaseAdapter {

    private final Activity context;
    final List<String> listUID;
    final List<Long> rankUID;
    final List<Integer> positionUID;
    final String theyear;
    final String themonth;


    public RankingAdapter(Activity context, List<String> listUID, List<Long> rankUID, List<Integer> positionUID, String theyear, String themonth) {

        this.context = context;
        this.listUID = listUID;
        this.rankUID = rankUID;
        this.positionUID = positionUID;
        this.themonth = themonth;
        this.theyear = theyear;


    }


    @Override
    public int getCount() {
        return listUID.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View gridView;



            Log.i("View -->", " is null +" + position);

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_ranking_adapter, null);




            TextView ranking_points=gridView.findViewById(R.id.ranking_points);
            ranking_points.setText(""+rankUID.get(position));

            TextView ranking_place = gridView.findViewById(R.id.ranking_place);
            ranking_place.setText("#"+positionUID.get(position));
            final CircleImageView rankagentimage = gridView.findViewById(R.id.rankagentimage);
            final TextView ranking_agentname = gridView.findViewById(R.id.ranking_agentname);
            mDatabase.child(listUID.get(position)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    try {
                        PicassoCache.getPicassoInstance(context)
                                .load(snapshot.getValue().toString()+"?sz=120")

                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(rankagentimage);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                }
            });
        mDatabase.child(listUID.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot2) {



                ranking_agentname.setText(snapshot2.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

            }
        });


        return gridView;

    }


}