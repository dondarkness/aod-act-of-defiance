package rocks.prxenon.aod.pages.ranking;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.RankingAdapter;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class RankingMain extends AppCompatActivity  implements AppBarLayout.OnOffsetChangedListener {

    private ProgressBar progressBarm, progress_gold, progress_platin,progress_black;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;

    private TextView mTitleTextView, subline, rank_anzahl_agents, rank_own_progress, onbadgestatus, badgetask, badge_statusleiste;
    private TextView missionname;
    private CircleImageView missionimage, missionimage2;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private AppBarLayout appbarlayout;
    private CollapsingToolbarLayout collapingtoolbar;
    private Toolbar toolbarx;
    private TextView agents_ownrank;
    private Bundle bundle;
    private ListView rankinglist;

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR  = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS     = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION              = 200;

    private boolean mIsTheTitleVisible          = false;
    private boolean mIsTheTitleContainerVisible = true;


    private LinearLayout closedframe;
    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private State mCurrentState = State.IDLE;



    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_ranking);



        bundle = getIntent().getExtras();


        if (bundle == null) {


            finish();

        }
        else {
            bindActivity();
            new AODConfig("default", this);
            //Get Firebase auth instance

            pref = getSharedPreferences("AppPref", MODE_PRIVATE);

            appbarlayout = findViewById(R.id.appbarlayout);
            collapingtoolbar = findViewById(R.id.collapingtoolbar);
            toolbarx = findViewById(R.id.toolbarx);
            closedframe = findViewById(R.id.closedframe);
            appbarlayout.addOnOffsetChangedListener(this);

            new AgentData("prefs", pref);
            type = Typeface.createFromAsset(getAssets(), "main.ttf");
            proto = Typeface.createFromAsset(getAssets(), "main.ttf");


            ActionBar mActionBar = getSupportActionBar();
            assert mActionBar != null;
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater mInflater = LayoutInflater.from(this);

            View actionBar = mInflater.inflate(R.layout.custom_actionbar_ranking, null);
            mTitleTextView = (TextView) actionBar.findViewById(R.id.title_text);
            subline = actionBar.findViewById(R.id.titlesub_text);
            mTitleTextView.setText(R.string.title_activity_ranking);
            mActionBar.setCustomView(actionBar);
            mActionBar.setDisplayShowCustomEnabled(true);
            ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


            mTitleTextView.setTypeface(type);
            mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

            if(bundle.getString("artis").equalsIgnoreCase("month")) {
                subline.setText(getResources().getString(R.string.month_mission_text));
            }


            /*
              iranking.putExtra("artis", "month");
                iranking.putExtra("theyear", theyearx);
                iranking.putExtra("themonth", themonthx);
                iranking.putExtra("theactionid", cmonthmissionid);
                iranking.putExtra("badgedrawable", monthbadgedrawable);
             */


            agents_ownrank = findViewById(R.id.agents_ownrank);
            rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
            rank_own_progress = findViewById(R.id.rank_own_progress);
            rankinglist = findViewById(R.id.rankinglist);
            missionname = (TextView) findViewById(R.id.missionname);
            missionimage = (CircleImageView) findViewById(R.id.missionimage);
            onbadgestatus = findViewById(R.id.onbadgestatus);
            progress_gold = findViewById(R.id.progress_gold);
            progress_platin = findViewById(R.id.progress_platin);
            progress_black = findViewById(R.id.progress_black);
            missionimage2 = (CircleImageView) findViewById(R.id.missionimage2);
            badgetask = findViewById(R.id.badgetask);
            badge_statusleiste = findViewById(R.id.badge_statusleiste);

            missionname.setTypeface(proto);
            missionname.setText(AgentData.getagentname());





            missionimage.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
            missionimage2.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));


            int badge_id = getResources().getIdentifier(bundle.getString("badgedrawable"), "drawable", getPackageName());

            //badgemonth = getResources().obtainTypedArray(badgemonthid);
            if (badge_id != 0) {
                //   Log.w("BADGE -->", " id :"+badgemonthid_content);
                //getDrawable(badge_id)

                // ImageView month_community_content = findViewById(R.id.content_community_month);
                // month_community_content.setImageDrawable(getDrawable(badge_id));

                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(badge_id)
                            .resize(400, 400)
                            .centerInside()
                            .placeholder(R.drawable.full_logo)
                            .error(R.drawable.full_logo)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(missionimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }
                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(badge_id)
                            .placeholder(R.drawable.full_logo)
                            .error(R.drawable.full_logo)
                            .resize(80, 80)
                            .centerInside()

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(missionimage2);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }
            }

        }



       // progressBar = (ProgressBar) findViewById(R.id.progressBar);

        // bgani = (ImageView) findViewById(R.id.bgani);

        startranking(bundle);

    }

    private void bindActivity() {

        closedframe = (LinearLayout)findViewById(R.id.closedframe);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;
        if (offset == 0) {
            Log.i("OFFSET1 --> ", ""+offset);
            setCurrentStateAndNotify(appBarLayout, State.EXPANDED);
           // handleAlphaOnTitle(percentage);
            closedframe.setVisibility(View.INVISIBLE);
        } else if (Math.abs(offset) >= appBarLayout.getTotalScrollRange()) {
            setCurrentStateAndNotify(appBarLayout, State.COLLAPSED);
          //  handleAlphaOnTitle(percentage);
            Log.i("OFFSET2 --> ", ""+offset);
            closedframe.setVisibility(View.VISIBLE);
        } else {
            Log.i("OFFSET3 --> ", ""+offset);
            setCurrentStateAndNotify(appBarLayout, State.IDLE);
        }
       // handleToolbarTitleVisibility(percentage);
    }

    private void setCurrentStateAndNotify(AppBarLayout appBarLayout, State state){
        if (mCurrentState != state) {
            onStateChanged(appBarLayout, state);
        }
        mCurrentState = state;
    }

    public void onStateChanged(AppBarLayout appBarLayout, State state) {

    }


    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if(mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }


    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();




    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {



            super.onBackPressed();


    }




  private void startranking(final Bundle bundleis) {
      // GET AGENTSDATA FOR THE MISSION
      Query myTopPostsQuery = mDatabaserunnung.child(bundleis.getString("theyear")).child(bundleis.getString("artis")).child(bundleis.getString("theactionid")).orderByChild("currentvalue");
      final HashMap<String, String> data = new HashMap<String, String>();
      final List<String> listUID = new ArrayList();
      final List<Long> rankUID = new ArrayList();
      final List<Integer> positionUID = new ArrayList();

      progress_gold.setMax(bundle.getInt("monthgold"));
      progress_platin.setMax(bundle.getInt("monthplatin"));
      progress_black.setMax(bundle.getInt("monthblack"));

      mDatabase.child(user.getUid()).child("badges").child(bundleis.getString("theactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot snapshotbadges) {

                if(snapshotbadges.hasChild("badge")) {
                    if(snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("GOLD")) {
                        onbadgestatus.setText("PLATIN");
                    }
                    else if(snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("PLATIN")) {
                        onbadgestatus.setText("BLACK");
                    }
                    else if(snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("BLACK")) {
                        onbadgestatus.setText("BLACK");
                    }
                }
                else {
                    onbadgestatus.setText("GOLD");
                }

              mDatabasemissions.child(bundleis.getString("artis")).child(bundleis.getString("theyear")).child(bundleis.getString("themonth")).addListenerForSingleValueEvent(new ValueEventListener() {
                  @Override
                  public void onDataChange(DataSnapshot snapshotmission) {

                      mDatabaseaod.child("badge").child(snapshotmission.child("badge").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                          @Override
                          public void onDataChange(DataSnapshot snapshotbadge) {

                              missionname.setText(snapshotbadge.child("title").getValue().toString());
                              badgetask.setText(snapshotbadge.child("short2").getValue().toString());
                              badge_statusleiste.setText(snapshotbadge.child("title").getValue().toString());


                          }

                          @Override
                          public void onCancelled(@NonNull DatabaseError databaseError) {
                              StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                          }
                      });


                  }

                  @Override
                  public void onCancelled(@NonNull DatabaseError databaseError) {
                      StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                  }
              });

          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {
              StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

          }
      });

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
          progress_gold.setProgress(0, true);
          progress_platin.setProgress(0, true);
          progress_black.setProgress(0, true);
      }
      else {
          progress_gold.setProgress(0);
          progress_platin.setProgress(0);
          progress_black.setProgress(0);
      }

       /*
              iranking.putExtra("artis", "month");
                iranking.putExtra("theyear", theyearx);
                iranking.putExtra("themonth", themonthx);
                iranking.putExtra("theactionid", cmonthmissionid);
                iranking.putExtra("badgedrawable", monthbadgedrawable);

                  iranking.putExtra("monthgold", goalgold);
        iranking.putExtra("monthplatin", goalplatin);
        iranking.putExtra("monthblack", goalblack);
             */

      // RANKING FOR THE AGENT
      myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot snapshot) {

              for (DataSnapshot child : snapshot.getChildren()) {
                  if(Long.parseLong(child.child("currentvalue").getValue().toString()) > 0) {
                      rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                      String test = child.child("uid").getValue().toString();
                      listUID.add(test);
                  }
              }
              Collections.reverse(listUID);
              Collections.reverse(rankUID);
              //  mList.indexOf(user.getUid())
              int ownpositioninList = listUID.indexOf(user.getUid());
              int ownrankis = 0;

              rank_anzahl_agents.setText(""+rankUID.size());
              if (ownpositioninList == -1) {
                  rank_own_progress.setText("-");
                  agents_ownrank.setText("-");
              } else {
                  for (int i = 0; i < rankUID.size(); i++) {
                      //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                      if (i == 0) {

                          ownrankis++;
                          positionUID.add(ownrankis);
                          if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                              if (rankUID.get(i) > 0) {
                                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                      progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                      progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                      progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                  }
                                  else {
                                      progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                      progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                      progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                  }
                                  rank_own_progress.setText(""+rankUID.get(i));
                                  //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                  agents_ownrank.setText("" + ownrankis);

                              } else {
                                  agents_ownrank.setText("-");
                                  rank_own_progress.setText("-");

                              }
                          }
                      } else {
                          if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                              ownrankis++;
                              positionUID.add(ownrankis);
                              if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                  //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                  if (rankUID.get(i) > 0) {
                                      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                          progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                          progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                          progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                      }
                                      else {
                                          progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                          progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                          progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                      }
                                      rank_own_progress.setText(""+rankUID.get(i));
                                      //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                      agents_ownrank.setText("" + ownrankis);
                                  } else {
                                      agents_ownrank.setText("-");
                                      rank_own_progress.setText("-");
                                  }
                              }
                          } else {
                              ownrankis++;
                              positionUID.add(ownrankis);
                              if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                  //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                  if (rankUID.get(i) > 0) {
                                      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                          progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                          progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                          progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                      }
                                      else {
                                          progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                          progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                          progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                      }
                                      //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                      agents_ownrank.setText("" + ownrankis);
                                      rank_own_progress.setText(""+rankUID.get(i));
                                  } else {
                                      agents_ownrank.setText("-");
                                      rank_own_progress.setText("-");
                                  }
                              }
                          }

                      }
                  }
                  //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                  if (ownrankis <= 0) {

                      agents_ownrank.setText("-");
                  }

              }

               /*
              iranking.putExtra("artis", "month");
                iranking.putExtra("theyear", theyearx);
                iranking.putExtra("themonth", themonthx);
                iranking.putExtra("theactionid", cmonthmissionid);
                iranking.putExtra("badgedrawable", monthbadgedrawable);
             */

              RankingAdapter adapter5 = new RankingAdapter(RankingMain.this,  listUID ,rankUID, positionUID, bundle.getString("theyear"), bundle.getString("themonth"));
              rankinglist.setAdapter(adapter5);
              adapter5.notifyDataSetChanged();

          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {
              StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

          }
      });
  }





}