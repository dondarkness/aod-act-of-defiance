package rocks.prxenon.aod.pages.profile;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.EaseEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.ramotion.foldingcell.FoldingCell;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.SplashScreen;
import rocks.prxenon.aod.adapter.HistoryUpdates;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.Fragment_ChangeLog;
import rocks.prxenon.aod.helper.OverlayScreenService;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.helper.ServiceCallBack;
import rocks.prxenon.aod.settings.MainSettings;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;


public class ProfileMain extends AppCompatActivity  implements AppBarLayout.OnOffsetChangedListener {

    private ProgressBar progressBar;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;

    private TextView mTitleTextView;
    private TextView agentname, agentname2;
    private CircleImageView agentimg, agentimg2;

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private AppBarLayout appbarlayout;
    private CollapsingToolbarLayout collapingtoolbar;
    private Toolbar toolbarx;
    private TextView ismail;

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR  = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS     = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION              = 200;

    private boolean mIsTheTitleVisible          = false;
    private boolean mIsTheTitleContainerVisible = true;
    private FancyButton profile_signout_btn;

    private LinearLayout closedframe;
    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private State mCurrentState = State.IDLE;



    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_profile);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        appbarlayout = findViewById(R.id.appbarlayout);
        collapingtoolbar = findViewById(R.id.collapingtoolbar);
        toolbarx = findViewById(R.id.toolbarx);
        closedframe = findViewById(R.id.closedframe);
        appbarlayout.addOnOffsetChangedListener(this);

        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        profile_signout_btn = findViewById(R.id.profile_signout_btn);
        profile_signout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.edit().remove("userexist").apply();
                AuthUI.getInstance()
                        .signOut(ProfileMain.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent i = new Intent(ProfileMain.this, FirebaseUIActivity.class);
                                i.putExtra("login", false);
                                startActivity(i);
                                finish();
                            }
                        });
            }
        });

        agentname = (TextView) findViewById(R.id.agentname);
        agentimg = (CircleImageView) findViewById(R.id.agentimage);
        agentname2 = (TextView) findViewById(R.id.agentname2);
        agentimg2 = (CircleImageView) findViewById(R.id.agentimage2);
        agentname.setTypeface(proto);
        agentname.setText(AgentData.getagentname());
        agentname2.setText(AgentData.getagentname());
        ismail = findViewById(R.id.ismail);
        ismail.setText(user.getEmail().toString());

        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorENL));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorENL));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorRES));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorRES));
        } else {
            agentimg.setBorderColor(getResources().getColor(R.color.colorGray));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorGray));
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getPhotoUrl().toString()+"?sz=400")
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getPhotoUrl().toString()+"?sz=80")
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg2);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_profile, null);
        mTitleTextView = (TextView) actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.title_activity_profile);
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);



       // progressBar = (ProgressBar) findViewById(R.id.progressBar);

        // bgani = (ImageView) findViewById(R.id.bgani);



    }

    private void bindActivity() {

        closedframe = (LinearLayout)findViewById(R.id.closedframe);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;
        if (offset == 0) {
            Log.i("OFFSET1 --> ", ""+offset);
            setCurrentStateAndNotify(appBarLayout, State.EXPANDED);
           // handleAlphaOnTitle(percentage);
            closedframe.setVisibility(View.INVISIBLE);
        } else if (Math.abs(offset) >= appBarLayout.getTotalScrollRange()) {
            setCurrentStateAndNotify(appBarLayout, State.COLLAPSED);
          //  handleAlphaOnTitle(percentage);
            Log.i("OFFSET2 --> ", ""+offset);
            closedframe.setVisibility(View.VISIBLE);
        } else {
            Log.i("OFFSET3 --> ", ""+offset);
            setCurrentStateAndNotify(appBarLayout, State.IDLE);
        }
       // handleToolbarTitleVisibility(percentage);
    }

    private void setCurrentStateAndNotify(AppBarLayout appBarLayout, State state){
        if (mCurrentState != state) {
            onStateChanged(appBarLayout, state);
        }
        mCurrentState = state;
    }

    public void onStateChanged(AppBarLayout appBarLayout, State state) {

    }


    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if(mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }


    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();


       /* if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorENL));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorRES));
        } else {
            agentimg.setBorderColor(getResources().getColor(R.color.colorGray));
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getPhotoUrl().toString())
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        Log.i("ONRESUM -->", "AODMain.class");
        initmissions();
     */

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {



            super.onBackPressed();


    }










}