package rocks.prxenon.aod.pages;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.EaseEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.ramotion.foldingcell.FoldingCell;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.IntroActivity;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.SplashScreen;
import rocks.prxenon.aod.adapter.HistoryUpdates;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.Fragment_ChangeLog;
import rocks.prxenon.aod.helper.OverlayScreenService;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.helper.ServiceCallBack;
import rocks.prxenon.aod.pages.profile.ProfileMain;
import rocks.prxenon.aod.pages.ranking.RankingMain;
import rocks.prxenon.aod.settings.MainSettings;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;


public class AODMain extends AppCompatActivity implements SearchView.OnClickListener, Animation.AnimationListener, ServiceCallBack {
    private static final int REQUEST_OVERLAY = 2;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int NUM_PAGES = 5;
    public static AlertDialog.Builder historybuilder;
    private static String[] PERMISSIONS_OVERLAY = {
            Manifest.permission.SYSTEM_ALERT_WINDOW
    };
    private static String[] PERMISSIONS_CAM = {android.Manifest.permission.CAMERA};
    // public DatabaseReference mDatabase;
    private static String[] PERMISSIONS_LOC = {Manifest.permission.ACCESS_COARSE_LOCATION};
    public HistoryUpdates historyadapter;
    public boolean hasdialogbefore;
    OverlayScreenService mService;
    boolean mBound = false;
    String themonthx;
    String theyearx;
    float initialX, initialY;
    private ProgressBar progressBar;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;
    private BoomMenuButton bmb_main;
    private ImageButton bmb_filter;
    private String[] menuitems, menusettingsone, menusettingstwo, menuitems_map;
    private TypedArray menuitems_drawble, menusettings_drawable, menuitems_drawble_map, badgemonth;
    private HamButton.Builder builder2, builder_filter;
    private TextView mTitleTextView;
    private TextView agentname;
    private CircleImageView agentimg;
    private AlertDialog.Builder dialogBuilder;
    private boolean cansave;
    private DataSnapshot snap;
    private ValueEventListener savewatch;
    private HashMap markerMap;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private CircleImageView imgagent;
    private FancyButton content_request_btn_month, content_request_btn_week;
    private FoldingCell fcweek, fcmonth;
    private RelativeLayout frameloadmonth, monthstartet, monthnotstartet, frameloadweek, weekstartet, weeknotstartet;
    private LinearLayout framecardmonth, framecardweek, communityframe;
    private FancyButton join_btn_week, join_btn_month;
    private String monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield, monthbadgedrawable, monthbadgedrawablegold, monthbadgedrawableplatin, monthbadgeid;
    private String cmonthmissionid, cweekmissionid;
    private CountDownTimer timer_month;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private SwipeRevealLayout swipe_layout_community;
    private ImageView iscommunitymission, close_btn_community;
    private String TAG = AODMain.class.getSimpleName();
    private ListView lv;
    private int themonthgoal, themonthgoalplatin,themonthgoalblack, getThemonthgoalcommunity, theweekgoal, themonthgoalcom, themonthgoalplatincom,themonthgoalblackcom;
    private SwipeRevealLayout swipe_team_month;
    private LinearLayout monthview_base;
    private ConstraintLayout baseframe;
    private ViewParent parent;
    private String lastVersion, thisVersion;
    private String workaround_actionid, workaround_month;
    private TickerView tickerView;
    private FrameLayout updateHolder;
    private int goalgold ,goalplatin,goalblack;
    private RelativeLayout action_ranking;


    // MAP MENUS
    private ServiceConnection connectionService = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(" VEG", "onServiceConnected");
            OverlayScreenService.LocalBinder scan = (OverlayScreenService.LocalBinder) service;
            mService = scan.getService();
            mService.setServiceCallBack(AODMain.this);
            mService.setCallbacks(AODMain.this); // register
            mService.startIngress();
            mBound = true;


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            Log.d("TAG", "onServiceDisconnected");
            mBound = false;
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    public static void verifyOverlay(Activity activity) {
        // Check if we have write permission
        if (!Settings.canDrawOverlays(activity)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, REQUEST_OVERLAY);
        }
    }

    public static String getMonthShortName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static String getMonthShorterName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_aod);
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        lastVersion = pref.getString("PREFS_VERSION_KEY", "");
        try {
            this.thisVersion = getPackageManager().getPackageInfo(getPackageName(),
                    0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            this.thisVersion = "";
            // Log.e(TAG, "could not get version name from manifest!");
            e.printStackTrace();
        }

        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        updateHolder = findViewById(R.id.updateHolder);
        mySwipeRefreshLayout = findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setRefreshing(false);

        mySwipeRefreshLayout.setProgressBackgroundColorSchemeColor(getApplicationContext().getResources().getColor(R.color.colorAccent, getTheme()));
        mySwipeRefreshLayout.setColorSchemeColors(getApplicationContext().getResources().getColor(R.color.colorWhite, getTheme()), getApplicationContext().getResources().getColor(R.color.colorPrimaryYellowLight, getTheme()), getApplicationContext().getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(true);
                        initmissions();


                    }
                }
        );

        baseframe = findViewById(R.id.baseframe);
        monthview_base = findViewById(R.id.monthview_base);
        tickerView = findViewById(R.id.tickerView);
        tickerView.setTypeface(proto);

        monthview_base.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                switch (action) {

                    case MotionEvent.ACTION_DOWN:
                        Log.d(TAG, "Action was DOWN");

                        break;


                    case MotionEvent.ACTION_CANCEL:
                        Log.d(TAG, "Action was CANCEL");
                        if (fcmonth.isUnfolded()) {
                            fcmonth.fold(false);
                        }

                        break;


                }

                return true;
            }
        });

        parent = baseframe.getParent();
        action_ranking = findViewById(R.id.action_ranking);
        action_ranking.setOnClickListener(this);

        frameloadmonth = findViewById(R.id.frameloadmonth);
        framecardmonth = findViewById(R.id.framecardmonth);
        monthstartet = findViewById(R.id.monthstartet);
        monthnotstartet = findViewById(R.id.monthnotstartet);

        frameloadweek = findViewById(R.id.frameloadweek);
        framecardweek = findViewById(R.id.framecardweek);
        weekstartet = findViewById(R.id.weekstartet);
        weeknotstartet = findViewById(R.id.weeknotstartet);
        /* Listeners */
        communityframe = findViewById(R.id.communityframe);
        swipe_team_month = findViewById(R.id.swipe_layout_2_month);
        // user = new API("default", this).user();
        // database = new API("default", this).database();
        // mDatabase = new API("default", this).mDatabase();
        content_request_btn_month = findViewById(R.id.content_request_btn_month);
        content_request_btn_week = findViewById(R.id.content_request_btn_week);
        iscommunitymission = findViewById(R.id.is_communitymission);
        iscommunitymission.setOnClickListener(this);
        close_btn_community = findViewById(R.id.close_btn_community);
        close_btn_community.setOnClickListener(this);
        content_request_btn_month.setOnClickListener(this);
        agentname = (TextView) findViewById(R.id.agentname);
        agentimg = (CircleImageView) findViewById(R.id.agentimage);
        agentname.setTypeface(proto);


        agentname.setText(AgentData.getagentname());
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar, null);
        mTitleTextView = (TextView) actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.app_name);
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        fcweek = findViewById(R.id.folding_cell_week);
        fcmonth = findViewById(R.id.folding_cell_month);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);
        bmb_filter = (ImageButton) actionBar.findViewById(R.id.bmb_filter);
        //bmb_settings = (BoomMenuButton) actionBar.findViewById(R.id.bmb_settings);


        bmb_filter.setOnClickListener(this);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        // bgani = (ImageView) findViewById(R.id.bgani);

        Log.i("ANDROID--->", "" + Build.VERSION.RELEASE);
        // load the animation
        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_in);

        animZoomNull = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_null);

        // set animation listener
        animZoomIn.setAnimationListener(this);
        animZoomNull.setAnimationListener(this);

        //bgani.startAnimation(animZoomIn);
        // button click event
        join_btn_week = findViewById(R.id.title_start_btn_week);
        join_btn_month = findViewById(R.id.title_start_btn_month);
        swipe_layout_community = findViewById(R.id.swipe_layout_community);


        findViewById(R.id.month_expanded_header).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                if (fcmonth.isUnfolded()) {
                    fcmonth.fold(false);
                }


                return true;
            }
        });

        findViewById(R.id.comframe).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);
                    communityframe.setVisibility(View.GONE);
                    swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                } else if (swipe_layout_community.isClosed()) {
                    swipe_layout_community.open(true);
                    swipe_team_month.setVisibility(View.GONE);
                    communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                }

                return true;
            }
        });

        findViewById(R.id.missionframe).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);
                    communityframe.setVisibility(View.GONE);
                    swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                } else if (swipe_layout_community.isClosed()) {
                    swipe_layout_community.open(true);
                    swipe_team_month.setVisibility(View.GONE);
                    communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                }

                return true;
            }
        });

        swipe_layout_community.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe.setVisibility(View.GONE);
                swipe_team_month.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {

                swipe_team_month.setVisibility(View.GONE);
                communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);
                fcmonth.requestLayout();
            }
        });

        bmb_main = (BoomMenuButton) actionBar.findViewById(R.id.bmb_main2);
        bmb_main.setButtonEnum(ButtonEnum.TextInsideCircle);
        bmb_main.setBoomInWholeScreen(true);
        bmb_main.setAutoHide(true);

        bmb_main.setBackgroundColor(getResources().getColor(R.color.transparent, getTheme()));
        bmb_main.setBackgroundResource(R.drawable.rounded_settings);
        bmb_main.setUse3DTransformAnimation(false);

        bmb_main.setDimColor(getResources().getColor(R.color.colorPrimaryDarkSemiTrans, getTheme()));
        menuitems = getResources().getStringArray(R.array.menutext);
        menuitems_drawble = getResources().obtainTypedArray(R.array.menutext_drawable);
        for (int i = 0; i < bmb_main.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    //.isRound(false)
                    .shadowCornerRadius(Util.dp2px(5))
                    //  .buttonCornerRadius(Util.dp2px(5))

                    .shadowEffect(true)
                    .imageRect(new Rect(50, 15, Util.dp2px(60), Util.dp2px(60)))
                    .imagePadding(new Rect(10, 15, 10, 8))
                    .textSize(13)
                    .highlightedColorRes(R.color.colorAccent)
                    .normalImageRes(menuitems_drawble.getResourceId(i, -1))
                    .normalText(menuitems[i])
                    .rippleEffect(true)

                    .typeface(proto)

                    .textPadding(new Rect(5, 4, 5, 0))
                    .normalColorRes(R.color.colorPrimaryDark)

                    .pieceColorRes(R.color.transparent);


            if (i == 3) {
                if (AgentData.getFaction() != null) {
                    if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                        builder.normalColorRes(R.color.colorENL);
                    } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                        builder.normalColorRes(R.color.colorRES);
                    } else {
                        builder.normalColorRes(R.color.colorAccent);
                    }
                } else {
                    Intent ix = new Intent(getBaseContext(), FirebaseUIActivity.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);
                    finish();
                }

            }

            bmb_main.addBuilder(builder);
            bmb_main.setOnBoomListener(new OnBoomListener() {


                @Override
                public void onClicked(int index, BoomButton boomButton) {

                    // TODO
                    if (index == 3) {
                        Intent i = new Intent(getBaseContext(), ProfileMain.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);

                    }
                    if (index == 4) {
                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);
                        finish();
                    }

                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {
                    // bmb_settings.setVisibility(View.GONE);
                    // bmb_filter.setVisibility(View.VISIBLE);
                    mTitleTextView.setText("AOD");
                }

                @Override
                public void onBoomDidHide() {

                }


                @Override
                public void onBoomWillShow() {
                    // bmb_settings.setVisibility(View.VISIBLE);
                    //     bmb_filter.setVisibility(View.GONE);
                    mTitleTextView.setText("AOD ..MENU");
                }

                @Override
                public void onBoomDidShow() {

                }


            });
        }


        // Log.i("AGENTNAME", pref.getString(API.child1,"null"));
        bmb_main.setBoomEnum(BoomEnum.LINE);
        bmb_main.setShowScaleEaseEnum(EaseEnum.EaseOutBack);


        if (user != null) {

            Crashlytics.setUserIdentifier("" + AgentData.getagentname());
            // initmissions();
            // new DataSetInit().execute();
            // User is signed in
        } else {
            startActivity(new Intent(this, FirebaseUIActivity.class));
            finish();
        }

        if (firstRun()) {
            getLogDialog();

        }


    }

    private void initmissions() {
        // TODO
        frameloadmonth.setVisibility(View.VISIBLE);
        framecardmonth.setVisibility(View.GONE);
        // GET THE DATE
        // SimpleDateFormat fday = new SimpleDateFormat("yyyyMMdd_HHmmss");
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
        final String themonth = fmonth.format(new Date());
        final String theyear = fyear.format(new Date());
        theyearx = theyear;
        themonthx = themonth;
        DateFormat format = new SimpleDateFormat("dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        final String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        Calendar sDateCalendar = new GregorianCalendar(Integer.parseInt(theyear),Integer.parseInt(themonth),Integer.parseInt(days[0]));
        int currentweek = sDateCalendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        String cweeksend;
        if(currentweek <= 9) {
            currentweek = Integer.parseInt("0"+currentweek);
            cweeksend = "0"+currentweek;
        }
        else {
            cweeksend = String.valueOf(currentweek);
        }

        Log.i("CURRENT WEEK FOR", ""+cweeksend);
        final String thefirstweekday = cweeksend + "" + days[0];
        Log.i("MONTH -->", themonth);
        Log.i("Year -->", theyear);
        Log.i("Dayof Week -->", "" + days[0]);

        mDatabase.child(user.getUid()).child("missions").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {


                if (snapshot.child("month").hasChild(themonth)) {
                    Log.i("MONTH -->", "Monat gefunden");

                    // STARTE MONAT GEFUNDEN
                    initmonth(theyear, themonth, true);


                } else {
                    Log.i("MONTH -->", "Kein Monat gefunden");
                    initmonth(theyear, themonth, false);


                }

                if (snapshot.child("week").hasChild(thefirstweekday)) {
                    fcweek.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fcweek.toggle(false);
                        }
                    });
                    initweek(theyear, themonth, thefirstweekday, days, true);
                    Log.i("WEEK -->", "Woche gefunden");

                } else {

                    initweek(theyear, themonth, thefirstweekday, days, false);
                    Log.i("WEEK -->", "Keine Woche gefunden");


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        mySwipeRefreshLayout.setRefreshing(false);

    }


    // JOIN A MISSION

    private void initmonth(final String theyear, final String themonth, final boolean isrunning) {
        Log.i("INIT MONTH -->", "gestartet " + isrunning);
        mDatabasemissions.child("month").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                Log.i("INIT MONTH SNAP -->", themonth + " " + theyear);


                if (snapshot.hasChild(themonth)) {
                    Log.i("MONTH SNAP -->", "Monat gefunden");
                    // SET TITLEVIEW
                    int monthd = 0;

                    try {
                        monthd = Integer.parseInt(themonth);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    TextView month_txt = findViewById(R.id.title_time_label);
                    month_txt.setText(getMonthShortName(monthd - 1));
                    //final TextView month_txt_content = findViewById(R.id.monthstitle);
                    final TextView month_txt_cat = findViewById(R.id.monthscat);


                    mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotaod) {
                            TextView monatsub = findViewById(R.id.title_to_address);
                            monatsub.setText(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString());
                            // monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;
                            monthbadgeid = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("id").getValue().toString();
                            monthbadgetitle = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString();
                            monthbadgeshort1 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString();
                            monthbadgeshort2 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short2").getValue().toString();
                            monthbadgefield = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("field").getValue().toString();
                            monthbadgedrawable = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString();
                            monthbadgedrawablegold = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawablegold").getValue().toString();
                            monthbadgedrawableplatin = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawableplatin").getValue().toString();
                            month_txt_cat.setText(snapshotaod.child("cat").child(snapshot.child(themonth).child("cat").getValue().toString()).child("title").getValue().toString());
                            TextView notstartedmonthinfo = findViewById(R.id.notstartedmonthinfo);

                            Log.i("Badges -->", "1 :"+monthbadgedrawable+" 2: "+monthbadgedrawablegold+ "3: "+monthbadgedrawableplatin);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                notstartedmonthinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshot.child(themonth).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                notstartedmonthinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshot.child(themonth).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString())));

                            }

                            themonthgoal = Integer.parseInt(snapshot.child(themonth).child("goal").getValue().toString());
                            themonthgoalplatin = Integer.parseInt(snapshot.child(themonth).child("goalplatin").getValue().toString());
                            themonthgoalblack = Integer.parseInt(snapshot.child(themonth).child("goalblack").getValue().toString());


                            //notstartedmonthinfo.setText("Ziel: "+snapshot.child(themonth).child("goal").getValue().toString() +" "+snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString());
                            // snapshot.child(themonth).child("id").getValue().toString()

                            // SET ICON

                            int badgemonthid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                            if (badgemonthid != 0) {
                                Log.w("BADGE -->", " id :" + badgemonthid);
                                ImageView month_badge = findViewById(R.id.title_price);
                                month_badge.setImageDrawable(getDrawable(badgemonthid));
                            }

                            // STARTE GET DATA FOR MONTH
                            if (isrunning) {
                                // TODO GET DETAILS FOR THE AGENT
                                cmonthmissionid = snapshot.child(themonth).child("id").getValue().toString();
                                monthgetagentDetails(snapshot.child(themonth).child("id").getValue().toString(), snapshot.child(themonth), theyear, themonth);

                            } else {

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        frameloadmonth.setVisibility(View.GONE);
                                        framecardmonth.setVisibility(View.VISIBLE);

                                    }
                                }, 1000);
                                Log.i("DATA ID -->", snapshot.child(themonth).child("id").getValue().toString());

                                monthnotstartet.setVisibility(View.VISIBLE);
                                monthstartet.setVisibility(View.GONE);
                                join_btn_month.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                            mDatabasemissions.child("month").child(theyear).child(themonth).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                    int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                    final Map<String, Object> childUpdatesx = new HashMap<>();
                                                    childUpdatesx.put("/agents", upagents);
                                                    mDatabasemissions.child("month").child(theyear).child(themonth).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                joinmission("month", snapshot.child(themonth).child("id").getValue().toString(), snapshot);
                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                errorToast("month", "9001");
                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                errorToast("month", "9002");
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                            errorToast("month", "9003");

                                                            //signOut();
                                                        }
                                                    });


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.e("TAG", "Failed to update.", error.toException());
                                                    errorToast("month", "9004");
                                                }
                                            });

                                        } else {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                            builder.setView(dialog);
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            // input.setAllCaps(true);
                                            input.setTypeface(type);

                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                            //localTextView1.setTypeface(proto);
                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    // Log.i("Hallo", "hallo");
                                                    //handleinputcode(input.getText().toString());
                                                    // Todo publish the code to database
                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    startActivity(i);
                                                    finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }

                                    }
                                });
                            }


                        }



                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });





                } else {

                    Log.i("MONTH SNAP-->", "Monat noch nicht aktiv");
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });


    }

    private void initweek(final String theyear, final String months, final String theweek, final String[] days, final boolean isrunning) {
        Log.i("INIT WEEK-->", "gestartet " + isrunning);
        mDatabasemissions.child("week").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                Log.i("INIT WEEK SNAP -->", theweek + " " + theyear);


                if (snapshot.hasChild(theweek)) {
                    Log.i("WEEK SNAP -->", "Woche gefunden");
                    // SET TITLEVIEW
                    int monthd = 0;

                    try {
                        monthd = Integer.parseInt(months);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    TextView week_txt2 = findViewById(R.id.title_date_label_week);
                    TextView week_txt1 = findViewById(R.id.title_time_label_week);
                    Calendar sDateCalendar = new GregorianCalendar(Integer.parseInt(theyear),monthd,Integer.parseInt(days[0]));
                    int currentweek = sDateCalendar.getInstance().get(Calendar.WEEK_OF_YEAR);
                    int monthn = sDateCalendar.getInstance().get(Calendar.MONTH);
                    monthd = monthn;
                    String cweeksend;

                    if (Integer.parseInt(days[6]) < Integer.parseInt(days[0])) {
                        week_txt1.setText(days[6] + " " + getMonthShorterName(monthn));
                    } else {
                        week_txt1.setText(days[6] + " " + getMonthShorterName(monthn - 1));
                    }
                    week_txt2.setText(days[0] + " " + getMonthShorterName(monthn - 1) + " -");
                    mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotaod) {
                            TextView weeksub = findViewById(R.id.title_to_address_week);
                            weeksub.setText(snapshotaod.child("badge").child(snapshot.child(theweek).child("badge").getValue().toString()).child("title").getValue().toString());

                            TextView notstartedweekinfo = findViewById(R.id.notstartedweekinfo);
                            notstartedweekinfo.setText(getString(R.string.goal)+": " + snapshot.child(theweek).child("goal").getValue().toString() + " " + snapshotaod.child("badge").child(snapshot.child(theweek).child("badge").getValue().toString()).child("short1").getValue().toString());
                            // snapshot.child(themonth).child("id").getValue().toString()

                            // SET ICON

                            int badgeweekid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshot.child(theweek).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                            if (badgeweekid != 0) {
                                Log.w("BADGE -->", " id :" + badgeweekid);
                                ImageView week_badge = findViewById(R.id.title_price_week);
                                week_badge.setImageDrawable(getDrawable(badgeweekid));
                            }


                        }

                        ;

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });


                    // STARTE GET DATA FOR MONTH
                    if (isrunning) {
                        // TODO GET DETAILS FOR THE AGENT
                        //cweekmissionid = snapshot.child(themonth).child("id").getValue().toString();
                        weeknotstartet.setVisibility(View.GONE);
                        weekstartet.setVisibility(View.VISIBLE);
                        // TODO WEEK DETAILS
                        fcweek.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                fcweek.toggle(false);
                            }
                        });
                        // TODO
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                frameloadweek.setVisibility(View.GONE);
                                framecardweek.setVisibility(View.VISIBLE);

                            }
                        }, 1200);
                    } else {

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                frameloadweek.setVisibility(View.GONE);
                                framecardweek.setVisibility(View.VISIBLE);

                            }
                        }, 1000);
                        Log.i("DATA ID -->", snapshot.child(theweek).child("id").getValue().toString());

                        weeknotstartet.setVisibility(View.VISIBLE);
                        weekstartet.setVisibility(View.GONE);

                        join_btn_week.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                    infoToast(getString(R.string.underconstruction));
                                    //   joinmission("week", snapshot.child(theweek).child("id").getValue().toString(), snapshot);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                    // builder.setMessage("Text not finished...");
                                    LayoutInflater inflater = getLayoutInflater();
                                    View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                    builder.setView(dialog);
                                    final TextView input = dialog.findViewById(R.id.caltext);
                                    // input.setAllCaps(true);
                                    input.setTypeface(type);

                                    //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                    //localTextView1.setTypeface(proto);
                                    builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            // Log.i("Hallo", "hallo");
                                            //handleinputcode(input.getText().toString());
                                            // Todo publish the code to database
                                            Intent i = new Intent(getBaseContext(), MainSettings.class);
                                            //i.putExtra("PersonID", personID);
                                            startActivity(i);
                                            finish();

                                        }
                                    });

                                    builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {


                                            // Todo publish the code to database
                                        }
                                    });

                                    builder.setCancelable(true);

                                    if (!isFinishing()) {
                                        builder.create().show();

                                    }
                                }
                            }
                        });

                    }


                } else {

                    Log.i("WEEK SNAP-->", "Week noch nicht aktiv");
                    fcweek.setVisibility(View.GONE);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });


    }

    private void joinmission(final String isart, final String missionid, final DataSnapshot snapshot) {
        // PROOF INGRESS SCREEN IS KALIBRATED
        Log.i("JOIN MISSION -->", missionid);
        bmb_main.setEnabled(false);
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
        final String themonth = fmonth.format(new Date());
        final String theyear = fyear.format(new Date());
        DateFormat format = new SimpleDateFormat("dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        final String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        final String thefirstweekday = themonth + "" + days[0];
        //   Log.i("MONTH -->", themonth);
        //   Log.i("Year -->", theyear);
        //   Log.i("Dayof Week -->", "" + days[0]);
        // START JOIN MISSION


        if (isart.equalsIgnoreCase("week")) {
            frameloadweek.setVisibility(View.VISIBLE);
            framecardweek.setVisibility(View.GONE);

            TextView loading_text = findViewById(R.id.weekload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_month.setEnabled(false);


            //mDatabase.child(user.getUid()).child("missions").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(thefirstweekday).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, themonth, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });

        }
        if (isart.equalsIgnoreCase("month")) {
            frameloadmonth.setVisibility(View.VISIBLE);
            framecardmonth.setVisibility(View.GONE);
            TextView loading_text = findViewById(R.id.monthload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_week.setEnabled(false);

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(themonth).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, themonth, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });
        }


    }

    private void addtomission(final DataSnapshot snapshot, final String theyear, final String themonth, final String thefirstweekday, final String isart, final String missionid) {
        final Map<String, Object> childUpdates = new HashMap<>();
        //Log.w("RESULTS -> ", "gid: " + gid + "user: " + user.getUid() + " profil " + aprofilimage);
        mDatabaserunnung.child(theyear).child(isart).child(missionid).child(user.getUid()).child("timestamp").setValue(System.currentTimeMillis() / 1000).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // IF hasmissions
                    childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                    childUpdates.put("/uid", user.getUid());

                    childUpdates.put("/googleid", user.getProviderData().get(1).getUid());
                    childUpdates.put("/startvalue", 0);
                    childUpdates.put("/lastvalue", 0);
                    childUpdates.put("/currentvalue", 0);
                    childUpdates.put("/finished", false);


                    mDatabaserunnung.child(theyear).child(isart).child(missionid).child(user.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                successfulJoined(snapshot, isart, missionid, theyear, thefirstweekday, themonth);
                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");
                                errorToast(isart, "3001");
                            } else {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");
                                errorToast(isart, "3002");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");
                            errorToast(isart, "3003");

                            //signOut();
                        }
                    });

                }
            }


        });

    }

    private void successfulJoined(DataSnapshot snapshot, String isart, String missionid, String theyear, String thefirstweekday, String themonth) {
        bmb_main.setEnabled(true);

        if (isart.equalsIgnoreCase("week")) {
            frameloadweek.setVisibility(View.GONE);
            framecardweek.setVisibility(View.VISIBLE);


            join_btn_month.setEnabled(true);
        } else if (isart.equalsIgnoreCase("month")) {
            frameloadmonth.setVisibility(View.GONE);
            framecardmonth.setVisibility(View.VISIBLE);


            join_btn_week.setEnabled(true);
        }

        initmissions();

    }

    private void monthgetagentDetails(final String theid, final DataSnapshot snapofdetails, final String theyear, final String themonth) {
        Log.i("THE ID OF EVENT -->", theid);
        Log.i("THE GOAL OF EVENT -->", snapofdetails.child("goal").getValue().toString());
        // INIT DETAILS
        boolean hasranking = Boolean.parseBoolean(snapofdetails.child("ranking").getValue().toString());
        boolean hasprice = Boolean.parseBoolean(snapofdetails.child("price").getValue().toString());
        boolean hasteam = Boolean.parseBoolean(snapofdetails.child("team").getValue().toString());
        boolean hasduell = Boolean.parseBoolean(snapofdetails.child("duell").getValue().toString());
        boolean hasxf = Boolean.parseBoolean(snapofdetails.child("xf").getValue().toString());
        int activeagents = Integer.parseInt(snapofdetails.child("agents").getValue().toString());
        goalgold = Integer.parseInt(snapofdetails.child("goal").getValue().toString());
        goalplatin= Integer.parseInt(snapofdetails.child("goalplatin").getValue().toString());
        goalblack = Integer.parseInt(snapofdetails.child("goalblack").getValue().toString());


        // INIT TEXT VIEWS
        final TextView goal_txt_title = findViewById(R.id.title_pledge);
        final TextView content_avatar_title_month = findViewById(R.id.content_avatar_title_month);
        final TextView content_community_title_month = findViewById(R.id.content_community_title_month);
        final TextView rank_txt_title = findViewById(R.id.title_weight);
        final TextView rank_txt_title_action = findViewById(R.id.rang_title_action);
        rank_txt_title.setOnClickListener(this);
        rank_txt_title_action.setOnClickListener(this);
        final TextView progress_txt_title = findViewById(R.id.title_requests_count);
        final TextView startwert_txt_content = findViewById(R.id.content_from_address_1_month);
        final TextView short2_txt_content = findViewById(R.id.content_from_address_2_month);
        final TextView rank_text_content = findViewById(R.id.content_to_address_2_month);
        final TextView rank_text_content_com = findViewById(R.id.content_to_address_2_month_com);
        final TextView goal_txt_content = findViewById(R.id.content_to_go_1_month);
        final TextView togo_txt_content = findViewById(R.id.content_to_go_2_month);
        final TextView togo_txt_content_com = findViewById(R.id.content_to_go_2_month_com);
        final TextView title_txt_content = findViewById(R.id.content_name_view_month);
        final TextView title_txt_community = findViewById(R.id.content_community_view_month);
        final TextView mission_detals_content_txt_month = findViewById(R.id.mission_detals_content_txt_month);
        final TextView mission_community_content_txt_month = findViewById(R.id.mission_community_content_txt_month);
        final TextView progress_txt_content = findViewById(R.id.content_to_address_1_month);
        final TextView goal_txt_content_community = findViewById(R.id.content_to_go_1_month_com);
        final TextView progress_txt_content_com = findViewById(R.id.content_to_address_1_month_com);
        final TextView last_upload_val_txt_content = findViewById(R.id.content_delivery_time_month);
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_month);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_month);
        final TextView lastupload_time_content = findViewById(R.id.content_delivery_date_month);
        final ProgressBar community_enl = findViewById(R.id.statusenl);
        final ProgressBar community_res = findViewById(R.id.statusres);
        final ProgressBar status_agent = findViewById(R.id.statusagent);
        final ImageView team_swipe_month = findViewById(R.id.teamswipe_month);

        final RelativeLayout monthhistory_action = findViewById(R.id.monthhistory_action);
        goal_txt_title.setText(snapofdetails.child("goal").getValue().toString());


        themonthgoalcom = Integer.parseInt(snapofdetails.child("community").child("goal").getValue().toString());
        themonthgoalplatincom = Integer.parseInt(snapofdetails.child("community").child("goalplatin").getValue().toString());
        themonthgoalblackcom = Integer.parseInt(snapofdetails.child("community").child("goalblack").getValue().toString());




        // SET DETAILS COMMUNITY

        int goalrechenvaluecom;
        String goaltextvaluecom;
        String monthbadgetoshowcom;
        if(Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalcom)  {
            goalrechenvaluecom = themonthgoalcom;
            monthbadgetoshowcom = monthbadgedrawablegold;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Gold"));



        }
        else if(Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalplatincom)  {
            goalrechenvaluecom = themonthgoalplatincom;
            monthbadgetoshowcom = monthbadgedrawableplatin;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Platin"));

        }
        else if(Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalblackcom)  {
            goalrechenvaluecom = themonthgoalblackcom;
            monthbadgetoshowcom = monthbadgedrawable;

            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Black"));
        }
        else {
            goalrechenvaluecom = themonthgoalblackcom;
            monthbadgetoshowcom = monthbadgedrawable;


        }

        Log.i("RESULT COMM -->", "g "+goalrechenvaluecom+" badge "+monthbadgetoshowcom);
        goal_txt_content_community.setText(""+goalrechenvaluecom);

        progress_txt_content_com.setText(snapofdetails.child("community").child("current").getValue().toString());

        community_enl.setMax(goalrechenvaluecom);
        community_res.setMax(goalrechenvaluecom);
        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
        int action_value2 = Arrays.asList(action_text2).indexOf(monthbadgeid); // pass value

        String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value2];

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, ""+goalrechenvaluecom, monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

        } else {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, ""+goalrechenvaluecom, monthbadgeshort1)));


        }

        int badgemonthid_content_com = getResources().getIdentifier(monthbadgetoshowcom, "drawable", getPackageName());

        //badgemonth = getResources().obtainTypedArray(badgemonthid);
        if (badgemonthid_content_com != 0) {
            //   Log.w("BADGE -->", " id :"+badgemonthid_content);

            ImageView month_community_content = findViewById(R.id.content_community_month);
            month_community_content.setImageDrawable(getDrawable(badgemonthid_content_com));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()), true);
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()), true);
        } else {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()));
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()));
        }
        int togo_com = goalrechenvaluecom - Integer.parseInt(snapofdetails.child("community").child("current").getValue().toString());
        //togo_txt_content_com.setText("noch "+togo_com);
        if (togo_com <= 0) {
            togo_com = Math.abs(togo_com);
            togo_txt_content_com.setText("Completed (+ " + togo_com + ")");
        } else {
            togo_txt_content_com.setText("noch " + togo_com);
        }
        rank_text_content_com.setText("Agents: " + snapofdetails.child("agents").getValue().toString());
        if (hasxf) {
            // TODO HAS XF = COMMUNITY MISSION
        }

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {


                if (snapshotteam.hasChild("team")) {
                    Log.i("TEAM-->", "Team gefunden");
                    //    team_month_exist.setVisibility(View.INVISIBLE);
                    team_swipe_month.setVisibility(View.VISIBLE);
                    swipe_team_month.setLockDrag(false);
                    // TODO INIT TEAM
                    // initmonth(theyear,themonth, true);


                } else {
                    Log.i("TEAM -->", "Kein TEAM gefunden");
                    // initmonth(theyear,themonth, false);
                    team_swipe_month.setVisibility(View.GONE);

                    swipe_team_month.setLockDrag(true);
                    swipe_layout_community.setLockDrag(false);

                    //  team_month_not_exist.setVisibility(View.INVISIBLE);


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // GET AGENTSDATA FOR THE MISSION
        Query myTopPostsQuery = mDatabaserunnung.child(theyear).child("month").child(theid).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));


                    String test = child.child("uid").getValue().toString();
                    listUID.add(test);

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);


                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                if (ownpositioninList == -1) {

                    rank_txt_title.setText("-");
                    rank_text_content.setText(getString(R.string.rang)+": ");
                } else {
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {
                            ownrankis++;
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                if (rankUID.get(i) > 0) {
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    rank_txt_title.setText("" + ownrankis);
                                    rank_text_content.setText(getString(R.string.rang)+": " + ownrankis);
                                } else {
                                    rank_txt_title.setText("-");
                                    rank_text_content.setText(getString(R.string.rang)+": -");
                                }
                            }
                        } else {
                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;

                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang)+": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang)+": -");
                                    }
                                }
                            } else {
                                ownrankis++;
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang)+": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang)+": -");
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        rank_txt_title.setText("-");
                        rank_text_content.setText(getString(R.string.rang)+": -");
                    }

                }

                //  mDatabaserunnung.child(theyear).child("month").child(theid)
                mDatabaserunnung.child(theyear).child("month").child(theid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotmissionagent) {
                        int goalrechenvalue;
                        String goaltextvalue;
                        String monthbadgetoshow;

                        if(Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalgold)  {
                            goalrechenvalue = goalgold;
                            monthbadgetoshow = monthbadgedrawablegold;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Gold"));
                            status_agent.setMax(goalgold);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));

                        }
                        else if(Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalplatin)  {
                            goalrechenvalue = goalplatin;
                            monthbadgetoshow = monthbadgedrawableplatin;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Platin"));
                            status_agent.setMax(goalplatin);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        else if(Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalblack)  {
                            goalrechenvalue = goalblack;
                            monthbadgetoshow = monthbadgedrawable;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Black"));
                            status_agent.setMax(goalblack);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        else {
                            goalrechenvalue = goalblack;
                            monthbadgetoshow = monthbadgedrawable;
                            status_agent.setMax(goalblack);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        goaltextvalue = String.valueOf(goalrechenvalue);



                        Long restvalue = (goalrechenvalue) - Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString());
                        goal_txt_content.setText(goaltextvalue);
                        goal_txt_title.setText(goaltextvalue);

                        if (restvalue <= 0) {
                            restvalue = Math.abs(restvalue);
                            togo_txt_content.setText(getString(R.string.txt_completed)+" (+ " + restvalue + ")");
                        } else {
                            togo_txt_content.setText(getString(R.string.txt_another)+" " + restvalue);
                        }
                        //  togo_txt_content.setText("noch "+restvalue);
                        progress_txt_title.setText(snapshotmissionagent.child("currentvalue").getValue().toString());
                        startwert_txt_content.setText(snapshotmissionagent.child("startvalue").getValue().toString());
                        short2_txt_content.setText(monthbadgeshort2);
                        title_txt_content.setText(monthbadgetitle);
                        title_txt_community.setText(monthbadgetitle);
                        progress_txt_content.setText(snapshotmissionagent.child("currentvalue").getValue().toString());

                        int badgemonthid_content = getResources().getIdentifier(monthbadgetoshow, "drawable", getPackageName());

                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgemonthid_content != 0) {
                            //   Log.w("BADGE -->", " id :"+badgemonthid_content);
                            ImageView month_badge_content = findViewById(R.id.content_avatar_month);
                            month_badge_content.setImageDrawable(getDrawable(badgemonthid_content));

                        }



                        String[] action_text = getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value = Arrays.asList(action_text).indexOf(monthbadgeid); // pass value
                        String action_value_text = getResources().getStringArray(R.array.badge_text_actions_value)[action_value];
                     //   String action_value_text_community = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value];


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        //    mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, ""+goalrechenvalue, monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                         //   mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1)));

                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, ""+goalrechenvalue, monthbadgeshort1)));

                        }
                        boolean hasoneupload;
                        if (Integer.parseInt(snapshotmissionagent.child("lastvalue").getValue().toString()) > 0) {
                            hasoneupload = true;
                        } else {
                            hasoneupload = false;
                        }
                        if (hasoneupload) {
                            last_upload_val_txt_content.setText(snapshotmissionagent.child("lastvalue").getValue().toString() + " " + monthbadgeshort1);
                        } else {
                            monthhistory_action.setVisibility(View.GONE);
                            last_upload_val_txt_content.setText(" - ");
                        }

                        int difftofinal = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) - Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                        Log.i("MONTHS -->", Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) + " " + Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                        String textrunsday;
                        if (difftofinal <= 1) {
                            if (difftofinal <= 0) {
                                textrunsday = getString(R.string.runtime_today)+"";
                            } else {
                                textrunsday = getString(R.string.runtime_oneday)+"";
                            }
                            String givenDateString = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + "." + theyear + " 23:59:59";
                            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            try {
                                Date mDate = sdf.parse(givenDateString);
                                long timeInMilliseconds = mDate.getTime();
                                Date currentDateis = new Date();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                                long diffInMs = mDate.getTime() - currentDateis.getTime();

                                System.out.println("diff:: " + diffInMs + " " + mDate.toString());
                                timer_month = new MonthFinish(diffInMs, 1000);
                                timer_month.start();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        } else {
                            textrunsday = difftofinal + " "+getString(R.string.string_days);
                            runtime_date_content.setText(getString(R.string.string_until)+ " " + Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + ". 0 Uhr");

                        }
                        deadlinetime_content.setText(textrunsday);


                        if (hasoneupload) {
                            ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            fcmonth.setLayoutParams(params);
                            fcmonth.requestLayout();
                            monthhistory_action.setVisibility(View.VISIBLE);
                            monthhistory_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openHistory(theyear, themonth, theid, "month", AODMain.this, null, -1);
                                    // infoToast("soon :-)");
                                }
                            });
                            Date oldDate = new Date((Long.parseLong(snapshotmissionagent.child("timestamp").getValue().toString()) * 1000));
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - oldDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            if (oldDate.before(currentDate)) {

                                Log.e("oldDate", "is previous date");
                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                        + " hours: " + hours + " days: " + days);
                                if (days >= 1) {
                                    if (days == 1) {

                                        lastupload_time_content.setText( String.format(getString(R.string.string_ago_day), ""+days));
                                    } else {
                                        lastupload_time_content.setText( String.format(getString(R.string.string_ago_days), ""+days));
                                    }
                                } else if (hours >= 1) {
                                    if (hours == 1) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hour), ""+hours));
                                    } else if (hours > 1 && hours < 23) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hours), ""+hours));
                                    } else {
                                        lastupload_time_content.setText(getString(R.string.string_oneday));
                                    }
                                } else if (minutes >= 0) {
                                    if (minutes == 0) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else if (minutes >= 1 && minutes <= 5) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_minutes), ""+minutes));
                                    }
                                }
                            } else {
                                lastupload_time_content.setText(getString(R.string.string_nouplaod));
                            }
                        } else {
                            rank_text_content.setText(getString(R.string.rang)+": -");
                            lastupload_time_content.setText(getString(R.string.string_nouplaod));
                        }

                        // Log.e("toyBornTime", "" + toyBornTime);

                        //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        monthnotstartet.setVisibility(View.GONE);
        monthstartet.setVisibility(View.VISIBLE);

        fcmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fcmonth.toggle(false);
                if (!fcmonth.isUnfolded()) {
                    final long now = SystemClock.uptimeMillis();
                    final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, fcmonth.getWidth() / 2, fcmonth.getHeight() / 2, 0);


                    swipe_layout_community.onTouchEvent(pressEvent);
                }


            }
        });
        // TODO GET DETAILS MONTH
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadmonth.setVisibility(View.GONE);
                framecardmonth.setVisibility(View.VISIBLE);

            }
        }, 1200);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation

        // check for zoom in animation
        if (animation == animZoomIn) {
            //  bgani.startAnimation(animZoomNull);
        }
        if (animation == animZoomNull) {
            //  bgani.startAnimation(animZoomIn);
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mBound) {
            Log.w("Service -_>", "Closed");
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
            if (isMyServiceRunning(OverlayScreenService.class)) {
                Log.i("SERVICESTATUS", "running");
                Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                stopService(svc);
            }
        }
        bmb_main.setAutoHide(false);
        if (bmb_main.isBoomed()) {
            bmb_main.reboom();
        }

        new AODConfig();


        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
        } else {
            agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getPhotoUrl().toString())
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

           new AODConfig("pommes", getApplicationContext());


            Log.i("ONRESUM -->", "AODMain.class");
        initmissions();


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (timer_month != null) {
            timer_month.cancel();
        }
        if (historybuilder != null) {
            hasdialogbefore = true;
            historybuilder.create().dismiss();
            historybuilder = null;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (timer_month != null) {
            timer_month.cancel();
        }
    }

    @Override
    public void onDestroy() {
        if (historybuilder != null) {
            hasdialogbefore = true;
            historybuilder.create().dismiss();
            historybuilder = null;
        }
        if (timer_month != null) {
            timer_month.cancel();
        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }

        if (isMyServiceRunning(OverlayScreenService.class)) {
            Log.i("SERVICESTATUS", "running");
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            stopService(svc);
        }
        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {

        if (progressBar.getVisibility() == View.VISIBLE) {
            Toast.makeText(getApplicationContext(), "Please wait... progress is running", Toast.LENGTH_SHORT).show();

        } else {
            pref.edit().remove("scanedagnet").apply();


            if (bmb_main.isBoomed()) {

                if (bmb_main.isBoomed()) {
                    bmb_main.reboom();
                }
            } else {

                super.onBackPressed();
            }
        }


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("RESPONSE -->", "" + requestCode + resultCode);

        if (requestCode == REQUEST_OVERLAY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // startscreenshot();
                    infoToast("Nochmal");

                }
            }
        }
        if (requestCode == 7879 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            Log.d("file", "FILE CHANGE OCCURED " + picturePath);


            data.putExtra("screen", picturePath);
            connectToDevicemanuel(data);
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }


    }

    private void startscreenshot(String month, String cmonthmissionid) {
        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(AODMain.this);

        if (prefs2.getBoolean("workaround", false)) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            workaround_actionid = month;
            workaround_month = cmonthmissionid;


            startActivityForResult(i, 7879);
        } else {
            Intent intent = new Intent(this, OverlayScreenService.class);
            bindService(intent, connectionService, Context.BIND_AUTO_CREATE);

            Log.i("SERVICESTATUS", "notrunning");
            //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield, monthbadgedrawable;
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0")) {
                svc.putExtra("ingress", "com.nianticlabs.ingress.prime.qa");
            } else {
                svc.putExtra("ingress", "com.nianticproject.ingress");
            }
            svc.putExtra("basistext", monthbadgetitle);
            svc.putExtra("activity", getCallingActivity());
            svc.putExtra("comp", monthbadgefield);
            svc.putExtra("agentname", AgentData.getagentname());
            svc.putExtra("mid", month);
            svc.putExtra("aid", cmonthmissionid);
            startService(svc);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        Intent iranking = new Intent(getBaseContext(), RankingMain.class);
        //i.putExtra("PersonID", personID);
        iranking.putExtra("artis", "month");
        iranking.putExtra("theyear", theyearx);
        iranking.putExtra("themonth", themonthx);
        iranking.putExtra("theactionid", cmonthmissionid);
        iranking.putExtra("badgedrawable", monthbadgedrawable);
        iranking.putExtra("monthgold", goalgold);
        iranking.putExtra("monthplatin", goalplatin);
        iranking.putExtra("monthblack", goalblack);
        // goalgold ,goalplatin,goalblack
        switch (v.getId()) {
            case R.id.bmb_filter:
                // mExplosionField.clear();
                Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.rang_title_action:
                // mExplosionField.clear();

                startActivity(iranking);
                //finish();
                break;
            case R.id.action_ranking:
                // mExplosionField.clear();

                startActivity(iranking);
                //finish();
                break;
            case R.id.title_weight:
                // mExplosionField.clear();

                startActivity(iranking);
                //finish();
                break;

            case R.id.is_communitymission:
                // mExplosionField.clear();
                if (swipe_layout_community.isClosed() && !swipe_layout_community.isDragLocked()) {
                    swipe_layout_community.open(true);

                    swipe_team_month.setVisibility(View.GONE);
                    communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                    fcmonth.requestLayout();


                }
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);
                    communityframe.setVisibility(View.GONE);
                    swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                    fcmonth.requestLayout();


                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.close_btn_community:
                // mExplosionField.clear();
                if (swipe_layout_community.isClosed() && !swipe_layout_community.isDragLocked()) {
                    swipe_layout_community.open(true);
                    communityframe.setVisibility(View.VISIBLE);
                    swipe_team_month.setVisibility(View.GONE);
                }
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);
                    communityframe.setVisibility(View.GONE);
                    swipe_team_month.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;

            case R.id.content_request_btn_month:
                if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                    // mExplosionField.clear();
                    if (isMyServiceRunning(OverlayScreenService.class)) {
                        Log.i("SERVICESTATUS", "running");
                        Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                        stopService(svc);
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (Settings.canDrawOverlays(AODMain.this)) {

                                startscreenshot("month", cmonthmissionid);
                                // startActivityForResult(intent , 7002);
                            } else {
                                verifyOverlay(AODMain.this);
                            }
                        } else {
                            startscreenshot("month", cmonthmissionid);
                            //   startActivityForResult(intent , 7002);
                        }
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                    // builder.setMessage("Text not finished...");
                    LayoutInflater inflater = getLayoutInflater();
                    View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                    builder.setView(dialog);
                    final TextView input = dialog.findViewById(R.id.caltext);
                    // input.setAllCaps(true);
                    input.setTypeface(type);

                    //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                    //localTextView1.setTypeface(proto);
                    builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Log.i("Hallo", "hallo");
                            //handleinputcode(input.getText().toString());
                            // Todo publish the code to database
                            Intent i = new Intent(getBaseContext(), MainSettings.class);
                            //i.putExtra("PersonID", personID);
                            startActivity(i);
                            finish();

                        }
                    });

                    builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            // Todo publish the code to database
                        }
                    });

                    builder.setCancelable(true);

                    if (!isFinishing()) {
                        builder.create().show();

                    }
                }
                break;

            case R.id.content_request_btn_week:
                // mExplosionField.clear();
                Toast.makeText(v.getContext(), "very soon", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void connectToDevice(Intent intent) {
        Log.d("IMAGE", " " + intent.getExtras().getString("screen"));
        if (isMyServiceRunning(OverlayScreenService.class)) {
            Log.i("SERVICESTATUS2", "running");
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            stopService(svc);
        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }


        String missionart = intent.getExtras().getString("onactionid");
        String onactionid = intent.getExtras().getString("missionart");
        Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        ContentResolver contentResolver = getContentResolver();
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
        recognizeText(image, onactionid, missionart);
        //connectToDevice(intent);
    }

    public void connectToDevicemanuel(Intent intent) {

        if (intent.getExtras().getString("screen") == null) {
            StyleableToast.makeText(getApplicationContext(), "Fehler: Der Screenshot muss Local gespeichert sein. Erstelle einen neuen Screenshot.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
        } else {
            Log.d("IMAGE", " " + intent.getExtras().getString("screen"));

            String missionart = workaround_actionid;
            String onactionid = workaround_month;
            workaround_month = null;
            workaround_actionid = null;
            Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
            ContentResolver contentResolver = getContentResolver();
            contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
            recognizeText(image, onactionid, missionart);
            //connectToDevice(intent);
        }
    }

    @Override
    public void closewindow() {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> rt = am.getRunningTasks(Integer.MAX_VALUE);
        Log.w("Back -->", "pressed " + rt.size());


        for (int i = 0; i < rt.size(); i++) {

            // bring to front
            if (rt.get(i).baseActivity.toShortString().indexOf("rocks.prxenon.aod") > -1) {
                am.moveTaskToFront(rt.get(i).id, ActivityManager.MOVE_TASK_WITH_HOME);
                Intent svc = new Intent(getApplicationContext(), OverlayScreenService.class);
                stopService(svc);
            }

        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }
    }

    private void recognizeText(FirebaseVisionImage image, final String onactionid, final String missionart) {

        // [START get_detector_default]
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        // [END get_detector_default]

        // [START run_detector]
        Task<FirebaseVisionText> result =
                detector.processImage(image)
                        .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                            @Override
                            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                // Task completed successfully
                                // [START_EXCLUDE]
                                // [START get_text]
                                boolean found = false;
                                String debugt = "null";
                                String resultis = "nothing";
                                int calint = pref.getInt("calint", 0);
                                Log.i("CALIBRATION --_>", "" + calint);
                                boolean correctbage = false;
                                boolean correctagent = false;
                                if (firebaseVisionText.getText().contains(monthbadgetitle)) {
                                    Log.i(monthbadgetitle + "--->", "yes");
                                    correctbage = true;
                                }

                                for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {

                                    Rect boundingBox = block.getBoundingBox();
                                    Point[] cornerPoints = block.getCornerPoints();
                                    String text = block.getText();
                                    text.replaceAll("\\s+", "");
                                    text = text.replace("o", "");
                                    text = text.replace("O", "");
                                    text = text.replace("l", "");
                                    text = text.replace("i", "");
                                    text = text.replace("I", "");
                                    text = text.replace("L", "");
                                    text = text.replace(",", "");
                                    text = text.replace(".", "");
                                    text = text.replace("0", "");
                                    text = text.replace("1", "");

                                  //  ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                   // ClipData clip = ClipData.newPlainText("AOD Debug", text.toString());
                                   // clipboard.setPrimaryClip(clip);

                                    String agentcleanded = AgentData.getagentname();
                                    agentcleanded = agentcleanded.replace("o", "");
                                    agentcleanded = agentcleanded.replace("O", "");
                                    agentcleanded = agentcleanded.replace("l", "");
                                    agentcleanded = agentcleanded.replace("i", "");
                                    agentcleanded = agentcleanded.replace("I", "");
                                    agentcleanded = agentcleanded.replace("L", "");
                                    agentcleanded = agentcleanded.replace(",", "");
                                    agentcleanded = agentcleanded.replace(".", "");
                                    agentcleanded = agentcleanded.replace("0", "");
                                    agentcleanded = agentcleanded.replace("1", "");
                                    agentcleanded = agentcleanded.substring(0, 3);
                                    if (text.contains(agentcleanded)) {
                                        Log.i(agentname + "--->", "yes");
                                        correctagent = true;
                                    }
                                    //  Log.w("Block-> ",block.getText()+""+block.getBoundingBox().top);
                                    for (FirebaseVisionText.Line line : block.getLines()) {
                                        // ...
                                        Log.w("Line-> ", line.getText() + " " + line.getBoundingBox().top);
                                        //if(block.getBoundingBox().top == calint) {
                                        Log.w("HERE-> ", line.getText() + " " + line.getBoundingBox().top + " " + calint);

                                        if (line.getBoundingBox().top >= calint - 5 && line.getBoundingBox().top <= calint + 5) {
                                            Log.i("IS IN RANGE ", "true");
                                            found = true;
                                            resultis = line.getText();
                                            debugt = "" + block.getBoundingBox().top;

                                        }

                                        else if (line.getBoundingBox().top + 5 >= calint - 5 && line.getBoundingBox().top + 5 <= calint + 5) {
                                            Log.i("IS IN RANGE ", "true");
                                            found = true;
                                            resultis = line.getText();
                                            debugt = "" + block.getBoundingBox().top;

                                        }


                                    }
                                }

                                if (found && correctbage && correctagent) {
                                    if (!debugt.equalsIgnoreCase("null")) {
                                        //  infoToast("DEBUG "+debugt);
                                    }

                                    Log.i("WOHOOO --->", resultis + " missionID:" + onactionid + " art:" + missionart);
                                    int theresult = 0;
                                    String toconvert = resultis.replaceAll("\\s+", "");
                                    toconvert = toconvert.replace("o", "0");
                                    toconvert = toconvert.replace("O", "0");
                                    toconvert = toconvert.replace("l", "1");
                                    toconvert = toconvert.replace("i", "1");
                                    toconvert = toconvert.replace("I", "1");
                                    toconvert = toconvert.replace("L", "1");
                                    toconvert = toconvert.replace(",", "");
                                    toconvert = toconvert.replace(".", "");
                                    toconvert = toconvert.replaceAll("[^\\d.]", "");
                                    try {
                                        theresult = Integer.parseInt(toconvert);

                                    } catch (NumberFormatException nfe) {
                                        System.out.println("Could not parse " + nfe);
                                    }

                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.value_string)+": " + theresult, Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                    writetomission(theresult, onactionid, missionart);
                                    // agentname_txt.setError("Es konnte kein Agent name gefunden werden.");
                                } else {
                                    if (correctbage && correctagent) {
                                        infoToast(getString(R.string.error_calibration_proof_main));
                                    } else if (!correctbage && correctagent) {
                                        infoToast(getString(R.string.error_wrong_badge)+" " + monthbadgetitle);
                                    } else if (correctbage && !correctagent) {
                                        infoToast(getString(R.string.error_wrong_agent_badge)+" " + AgentData.getagentname());
                                       // ACTION TRICKSTER

                                        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot snapshot) {
                                                int trickstercount = 0;

                                                if (snapshot.hasChild("tricksterstatus")) {

                                                    trickstercount = Integer.parseInt(snapshot.child("tricksterstatus").child("count").getValue().toString());



                                                final Map<String, Object> tricksterUpdates = new HashMap<>();
                                                // IF hasmissions
                                                    tricksterUpdates.put("/count", trickstercount+1);



                                                    mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            //finish();

                                                        } else if (task.isCanceled()) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                        } else {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                        }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                            //signOut();
                                                        }
                                                    });
                                                } else {
                                                    final Map<String, Object> tricksterUpdates = new HashMap<>();
                                                    // IF hasmissions
                                                    tricksterUpdates.put("/count", 1);



                                                    mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();

                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                            //signOut();
                                                        }
                                                    });
                                                }


                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                              //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            }
                                        });

                                    } else {
                                        infoToast(getString(R.string.error_badge_or_agent)+" " + monthbadgetitle + "  " + AgentData.getagentname());
                                        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot snapshot) {
                                                int trickstercount = 0;

                                                if (snapshot.hasChild("tricksterstatus")) {

                                                    trickstercount = Integer.parseInt(snapshot.child("tricksterstatus").child("count").getValue().toString());



                                                    final Map<String, Object> tricksterUpdates = new HashMap<>();
                                                    // IF hasmissions
                                                    tricksterUpdates.put("/count", trickstercount+1);



                                                    mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();

                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                            //signOut();
                                                        }
                                                    });
                                                } else {
                                                    final Map<String, Object> tricksterUpdates = new HashMap<>();
                                                    // IF hasmissions
                                                    tricksterUpdates.put("/count", 1);



                                                    mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();

                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                            //signOut();
                                                        }
                                                    });
                                                }


                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            }
                                        });

                                    }
                                }
                                // [END get_text]
                                // [END_EXCLUDE]
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // Task failed with an exception
                                        // ...
                                    }
                                });
        // [END run_detector]
    }

    private void writetomission(final int theresult, final String onactionid, final String missionart) {
        final Map<String, Object> childUpdates = new HashMap<>();
        final Map<String, Object> childUpdatesCommunity = new HashMap<>();
        final Map<String, Object> childMissionAgent = new HashMap<>();

        final Map<String, Object> childMissionBadgeAgent = new HashMap<>();

        final boolean setbadges = false;

        // IF hasmissions


        mDatabaserunnung.child(theyearx).child(missionart).child(onactionid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            int currentis;
            int updatetoast;
            @Override
            public void onDataChange(final DataSnapshot snapshotmissionvalues) {
                childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                childMissionAgent.put("/timestamp", System.currentTimeMillis() / 1000);
                final boolean initupdate;

                if (Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()) <= 0) {
                    childUpdates.put("/startvalue", theresult);
                    childUpdates.put("/lastvalue", theresult);
                    childMissionAgent.put("/startvalue", theresult);
                    childMissionAgent.put("/value", theresult);

                    initupdate = true;
                } else {

                    currentis = (theresult - Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()));

                    if (Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString()) > 0) {
                        updatetoast = currentis - Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString());
                    }
                    else {
                        updatetoast = currentis;
                    }
                    initupdate = false;
                    childUpdates.put("/lastvalue", theresult);
                    childUpdates.put("/currentvalue", currentis);
                    childMissionAgent.put("/value", theresult);
                    childMissionAgent.put("/startvalue", Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()));

                    if (currentis < themonthgoal) {
                        childMissionBadgeAgent.put("/badge", null);
                        childUpdates.put("/finished", false);
                    }
                    else if (currentis >= themonthgoal && currentis < themonthgoalplatin) {
                        // GOLD BADGE ERREICHT
                        childMissionBadgeAgent.put("/badge", "GOLD");

                    }
                    else if (currentis >= themonthgoalplatin && currentis < themonthgoalblack) {
                        // PLATIN BADGE ERREICHT
                        childMissionBadgeAgent.put("/badge", "PLATIN");

                    }
                    else if (currentis >= themonthgoalblack) {
                        childMissionBadgeAgent.put("/badge", "BLACK");
                        // BLACK BADGE ERREICHT

                    }



                }

                // UPDATE WERT > start und last update
                if (theresult > Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()) && theresult > Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString())) {


                    if(theresult > (Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString())*2 ) && !initupdate ) {
                        StyleableToast.makeText(getApplicationContext(), "Value is to high. " + theresult + " please try again.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                    else {
                        //Todo prüfen ob wert zu groß
                        updateHolder.setVisibility(View.VISIBLE);
                        tickerView.setCharacterLists(TickerUtils.provideNumberList());

                        tickerView.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        tickerView.setAnimationDuration(1500);
                        tickerView.setGravity(Gravity.CENTER_VERTICAL);

                        tickerView.setAnimationInterpolator(new OvershootInterpolator());
                        tickerView.setText("+ " +updatetoast, true);
                        // UPDATE COMMUNITY MISSION
                        mDatabasemissions.child(missionart).child(theyearx).child(themonthx).child("community").addListenerForSingleValueEvent(new ValueEventListener() {
                            int community_current, community_current_enl, community_current_res, rechenvalue;

                            public void onDataChange(DataSnapshot snapshotcommunityvalues) {


                                community_current = Integer.parseInt(snapshotcommunityvalues.child("current").getValue().toString());
                                community_current_enl = Integer.parseInt(snapshotcommunityvalues.child("enl").getValue().toString());
                                community_current_res = Integer.parseInt(snapshotcommunityvalues.child("res").getValue().toString());
                                if (initupdate) {

                                } else {
                                    if (Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString()) > 0) {
                                        rechenvalue = theresult - Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString());
                                        childUpdatesCommunity.put("/current", community_current + rechenvalue);
                                        //childUpdatesCommunity.put("/current", community_current + rechenvalue);
                                        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                                            childUpdatesCommunity.put("/enl", community_current_enl + rechenvalue);
                                        }
                                        if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                                            childUpdatesCommunity.put("/res", community_current_res + rechenvalue);
                                        }
                                    }
                                }


                                mDatabasemissions.child(missionart).child(theyearx).child(themonthx).child("community").updateChildren(childUpdatesCommunity).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();

                                        } else if (task.isCanceled()) {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                            errorToast("Error Community Mission", "40001");
                                        } else {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                            errorToast("Community Mission", "40002");
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                        errorToast("Community Mission", "40003");

                                        //signOut();
                                    }
                                });

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });
                        mDatabaserunnung.child(theyearx).child(missionart).child(onactionid).child(user.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();
                                    mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).child(newKey).updateChildren(childMissionAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                //finish();

                                                mDatabase.child(user.getUid()).child("badges").child(onactionid).updateChildren(childMissionBadgeAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();

                                                            new Handler().postDelayed(new Runnable() {

                                                                /*
                                                                 * Showing splash screen with a timer. This will be useful when you
                                                                 * want to show case your app logo / company
                                                                 */

                                                                @Override
                                                                public void run() {
                                                                    // This method will be executed once the timer is over
                                                                    // Start your app main activity

                                                                    updateHolder.setVisibility(View.GONE);
                                                                  //  tickerView.setText("+" +currentis, true);
                                                                    initmonth(theyearx, themonthx, true);
                                                                }
                                                            }, 2500);

                                                        } else if (task.isCanceled()) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                            errorToast("WRITE BADGE", "40001");
                                                        } else {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                            errorToast("WRITE BADGE", "40002");
                                                        }
                                                    }

                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(Exception e) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                                        errorToast("WRITE BADGE", "40003");

                                                        //signOut();
                                                    }
                                                });

                                            } else if (task.isCanceled()) {
                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                errorToast("Error", "10001");
                                            } else {
                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                errorToast(missionart, "10002");
                                            }
                                        }

                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                            errorToast(missionart, "10003");

                                            //signOut();
                                        }
                                    });
                                } else if (task.isCanceled()) {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                    errorToast("Error", "10001");
                                } else {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                    errorToast(missionart, "10002");
                                }
                            }

                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                errorToast(missionart, "10003");

                                //signOut();
                            }
                        });
                    }
                } else {


                    StyleableToast.makeText(getApplicationContext(), "Value is to small. " + theresult + " is not an improvement.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });


    }

    public void openHistory(final String theyear, final String themonth, final String theid, final String theartis, final Activity aodMain, final HistoryUpdates historyadapterx, int position) {
        LayoutInflater inflater = aodMain.getLayoutInflater();
        hasdialogbefore = false;
        final View dialog = inflater.inflate(R.layout.dialog_history, null);
        if (historybuilder == null) {
            historybuilder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);
            hasdialogbefore = false;
            // builder.setMessage("Text not finished...");

            historybuilder.setView(dialog);
            historybuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    hasdialogbefore = false;
                    if(historybuilder != null) {
                        historybuilder.create().dismiss();
                        historybuilder = null;
                    }
                }
            });
        } else {
            hasdialogbefore = true;
        }
        final List<Long> updatetime = new ArrayList<Long>();
        final List<String> updatevalue = new ArrayList<String>();
        final List<String> updatekey = new ArrayList<String>();

        mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshothistory) {
                String[] updatevalueset;
                Long[] updatetimeset;
                String[] updatekeyset;

                updatekeyset = null;
                updatetimeset = null;
                updatevalueset = null;
                updatetime.clear();
                updatekey.clear();
                updatevalue.clear();
                for (DataSnapshot historyis : snapshothistory.getChildren()) {
                    //  Log.i("timestamp", historyis.getValue().toString());
                    if (historyis.hasChildren()) {
                        updatetime.add(Long.parseLong(historyis.child("timestamp").getValue().toString()));
                        updatevalue.add(historyis.child("value").getValue().toString());
                        updatekey.add(historyis.getKey());

                        Log.i("timestamp ----->", historyis.child("timestamp").getValue().toString());
                    }
                }

                updatevalueset = new String[updatevalue.size()];
                updatetimeset = new Long[updatetime.size()];
                updatekeyset = new String[updatekey.size()];

                updatetimeset = updatetime.toArray(updatetimeset);
                updatevalueset = updatevalue.toArray(updatevalueset);
                updatekeyset = updatekey.toArray(updatekeyset);
                Collections.sort(Arrays.asList(updatetimeset), Collections.reverseOrder());
                Collections.sort(Arrays.asList(updatevalueset), Collections.reverseOrder());
                Collections.sort(Arrays.asList(updatekeyset), Collections.reverseOrder());


                if (historyadapterx == null) {
                    historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, monthbadgeshort1, monthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                    lv = dialog.findViewById(R.id.listhistory);
                    lv.setAdapter(historyadapter);
                    historyadapter.notifyDataSetChanged();
                } else {

                    historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, monthbadgeshort1, monthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                    historyadapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
        //localTextView1.setTypeface(proto);

        historybuilder.setPositiveButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                hasdialogbefore = false;
                if(historybuilder != null) {
                    historybuilder.create().dismiss();
                    historybuilder = null;
                }

            }
        });

        if (!hasdialogbefore) {
            historybuilder.setCancelable(true);

        }

        if (!isFinishing()) {
            if(historybuilder != null) {
                if (historybuilder.create().isShowing()) {
                    historybuilder.create().dismiss();
                } else {
                    if (!hasdialogbefore) {

                        historybuilder.create().show();
                    }
                }
            }

        }
    }



    public void removeHistory(final String keyi, final int position, final String theyear, final String themonth, final String theartis, final String theid, final Activity context, final HistoryUpdates historyadapterx) {
        StyleableToast.makeText(context, " " + keyi, Toast.LENGTH_LONG, R.style.defaulttoast).show();


        final Map<String, Object> updaterunning = new HashMap<>();


        // IF hasmissions


        mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(final DataSnapshot snapshotremove) {
                boolean hasmorechilds;
                boolean hasnchild;
                //HAS NEXT
                if (snapshotremove.getChildrenCount() > 0) {
                    hasnchild = true;
                    // HAS MORE Childs
                    if (snapshotremove.getChildrenCount() > (position + 2)) {
                        hasmorechilds = true;
                    }
                    // HAS NOT MORE CHilds
                    else {
                        hasmorechilds = false;
                    }
                }
                // HAS NO NEXT
                else {
                    hasmorechilds = false;
                    hasnchild = false;
                }

                // IST Das neuste und hat mehrere Childs
                if (position == 0 && hasmorechilds) {

                    int searchpos = 0;
                    String prekey = keyi;
                    String searchkey = "null";
                    for (DataSnapshot postSnapshot : snapshotremove.getChildren()) {
                        Log.i("KEY: pos", postSnapshot.getKey() + " ");
                        if (postSnapshot.hasChildren()) {
                            if (postSnapshot.getKey().equalsIgnoreCase(keyi)) {
                                searchkey = prekey;
                                Log.i("KEYs --> ", "this " + keyi + " davor: " + searchkey);
                            } else {
                                prekey = postSnapshot.getKey();
                            }
                        }
                    }

                    String valueforthis = snapshotremove.child(keyi).child("value").getValue().toString();
                    String valuenew = snapshotremove.child(searchkey).child("value").getValue().toString();
                    String valuestart = snapshotremove.child(keyi).child("startvalue").getValue().toString();
                    String valuerechnen = snapshotremove.child(keyi).child("value").getValue().toString();
                    Map<String, Object> updaterunning2 = new HashMap<>();
                    final int rechnung = Integer.parseInt(valuenew) - Integer.parseInt(valuestart);
                    final int rechnung2 = Integer.parseInt(valuerechnen) - Integer.parseInt(valuenew);
                    updaterunning2.put("/lastvalue", valuenew);
                    updaterunning2.put("/currentvalue", rechnung);
                    updaterunning2.put("/startvalue", snapshotremove.child(searchkey).child("startvalue").getValue().toString());
                    Log.i("USER --> ", " " + user.getUid());
                    Log.i("Data --> ", "y " + theyear + " art:" + theartis + " id: " + theid + " new: " + valuenew + " current:" + rechnung + " start:" + snapshotremove.child(searchkey).child("startvalue"));

                    // TODO Abzug All Community

                    mDatabaserunnung.child(theyear).child(theartis).child(theid).child(user.getUid()).updateChildren(updaterunning2).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                //openHistory(theyear,themonth,theid, theartis, context);
                                mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht und die Mission zurückgesetzt.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            hasdialogbefore = false;
                                            historybuilder.create().dismiss();
                                            historybuilder = null;

                                            removefromcommunity(rechnung2, theyear, theartis, theid, themonth, context);


                                        } else if (task.isCanceled()) {

                                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                                    }
                                });

                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10001");

                            } else {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10002");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", context.getClass().getSimpleName() + " 10003");


                            //signOut();
                        }
                    });

                }
                // IST das neuste und das eintige
                else if (position == 0 && !hasmorechilds) {

                    updaterunning.put("/lastvalue", 0);
                    updaterunning.put("/currentvalue", 0);
                    updaterunning.put("/startvalue", 0);
                    mDatabaserunnung.child(theyear).child(theartis).child(theid).child(user.getUid()).updateChildren(updaterunning).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                //openHistory(theyear,themonth,theid, theartis, context);
                                mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht und die Mission zurückgesetzt!", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            historybuilder.create().dismiss();
                                            hasdialogbefore = false;
                                            historybuilder = null;
                                            context.recreate();

                                        } else if (task.isCanceled()) {

                                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                                    }
                                });

                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10001");

                            } else {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10002");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", context.getClass().getSimpleName() + " 10003");


                            //signOut();
                        }
                    });
                } else if (position > 0) {

                    mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();

                                hasdialogbefore = false;
                                historybuilder.create().dismiss();
                                historybuilder = null;
                                StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                context.recreate();

                            } else if (task.isCanceled()) {

                                hasdialogbefore = false;
                                historybuilder.create().dismiss();
                                historybuilder = null;

                                Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {

                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(context, getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

    }

    public void removefromcommunity(final int rechnung, final String theyear, String theartis, String theid, final String themonth, final Activity context) {

        mDatabasemissions.child("month").child(theyear).child(themonth).child("community").addListenerForSingleValueEvent(new ValueEventListener() {
            int community_current, community_current_enl, community_current_res, rechenvalue, enlnew, resnew;

            public void onDataChange(DataSnapshot snapshotcommunityvalues) {
                final Map<String, Object> childUpdatesCommunity = new HashMap<>();

                community_current = Integer.parseInt(snapshotcommunityvalues.child("current").getValue().toString());
                community_current_enl = Integer.parseInt(snapshotcommunityvalues.child("enl").getValue().toString());
                community_current_res = Integer.parseInt(snapshotcommunityvalues.child("res").getValue().toString());


                rechenvalue = community_current - rechnung;

                childUpdatesCommunity.put("/current", rechenvalue);

                if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                    enlnew = community_current_enl - rechnung;
                    childUpdatesCommunity.put("/enl", enlnew);
                } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                    resnew = community_current_res - rechnung;
                    childUpdatesCommunity.put("/res", resnew);

                }

                Log.i("Update community -->", "n: " + rechenvalue + " enl " + enlnew + " res " + resnew + " rechen " + rechnung);


                mDatabasemissions.child("month").child(theyear).child(themonth).child("community").updateChildren(childUpdatesCommunity).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            context.recreate();
                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();

                        } else if (task.isCanceled()) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                            errorToast("Error Community Mission", "40001");
                            context.recreate();
                        } else {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                            errorToast("Community Mission", "40002");
                            context.recreate();
                        }
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                        errorToast("Community Mission", "40003");
                        context.recreate();
                        //signOut();
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                context.recreate();
            }
        });


    }

    public void updateVersionInPreferences(Context context, String thisVersion) {
        // save new version number to preferences

        //Log.e("TAG", " "+thisVersion);
        pref.edit().putString("PREFS_VERSION_KEY", thisVersion).apply();


    }

    private void openDialogFragment(DialogFragment dialogStandardFragment) {
        if (dialogStandardFragment != null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment prev = fm.findFragmentByTag("changelogdemo_dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            dialogStandardFragment.show(ft, "changelogdemo_dialog");
        }
    }

    private void getLogDialog() {
        openDialogFragment(new Fragment_ChangeLog());
    }

    private boolean firstRun() {
        return !this.lastVersion.equals(this.thisVersion);
    }

    private void errorToast(String text, String num) {
        bmb_main.setEnabled(true);
        join_btn_week.setEnabled(true);
        join_btn_month.setEnabled(true);
        frameloadmonth.setVisibility(View.GONE);
        framecardmonth.setVisibility(View.VISIBLE);
        frameloadweek.setVisibility(View.GONE);
        framecardweek.setVisibility(View.VISIBLE);
        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + num, Toast.LENGTH_LONG, R.style.defaulttoast).show();
    }

    private void infoToast(String num) {
        bmb_main.setEnabled(true);
        join_btn_week.setEnabled(true);
        join_btn_month.setEnabled(true);
        frameloadmonth.setVisibility(View.GONE);
        framecardmonth.setVisibility(View.VISIBLE);
        frameloadweek.setVisibility(View.GONE);
        framecardweek.setVisibility(View.VISIBLE);
        StyleableToast.makeText(getApplicationContext(), num, Toast.LENGTH_LONG, R.style.defaulttoast).show();
    }

    public class MonthFinish extends CountDownTimer {
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_month);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_month);
        String serverUptimeText;

        public MonthFinish(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            content_request_btn_month.setEnabled(false);
            content_request_btn_month.setText("Beendet");
            deadlinetime_content.setText("Mission Closed");
            runtime_date_content.setText("");
            //wrongscreenownescreen(linkid, isowner, "1");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Long timerup = millisUntilFinished / 1000;

            if (timerup / 86400 > 0) {
                serverUptimeText = String.format("%d d %d h %d min %d sec",
                        timerup / 86400,
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 > 0) {
                serverUptimeText = String.format("%d h %d min %d sec",
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 <= 0 && ((timerup % 86400) % 3600) / 60 > 0) {
                serverUptimeText = String.format("%d min %d sec",
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else {
                serverUptimeText = String.format("%d sec",
                        ((timerup % 86400) % 3600) % 60
                );
            }

            //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
            runtime_date_content.setText(serverUptimeText);
        }
    }
}