package rocks.prxenon.aod.basics;


import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import androidx.multidex.MultiDex;
import io.fabric.sdk.android.Fabric;


public class AplicationStart extends android.app.Application {
    public static FirebaseRemoteConfig remoteconfig;


    @Override
    public void onCreate() {
        super.onCreate();



        /* Enable disk persistence  */
        //  FirebaseDatabase.getInstance();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase.getInstance().goOnline();
        Fabric.with(this, new Crashlytics());


        remoteconfig = FirebaseRemoteConfig.getInstance();


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}