package rocks.prxenon.aod.basics;

import android.content.SharedPreferences;
import android.util.Log;


/**
 * Created by PrXenon on 04.09.2017.
 */

public class AgentData {

    public static int appversion;
    public static String agentname, faction;
    public static String trickster;


    public AgentData() {

    }

    public AgentData(String ref, int data) {
        Log.i("GREFFERER", "" + ref);
        if (ref.equalsIgnoreCase("appversion")) {

            appversion = data;
        }

    }

    public AgentData(String ref, String data) {
        Log.i("GREFFERER", "" + ref);
        if (ref.equalsIgnoreCase("agentname")) {

            agentname = data;
        }
        if (ref.equalsIgnoreCase("faction")) {

            faction = data;
        }

        if (ref.equalsIgnoreCase("trickster")) {

            trickster = data;
        }

    }

    public AgentData(String ref, SharedPreferences getpref) {
        AODConfig.pref = getpref;

    }

    public static int getappversion(String ref) {
        int returnstring = 0;
        if (ref.equalsIgnoreCase("appversion")) {
            //Log.i(" GET REQUEST I -->", "" + appversion);
            // return minversion;
            returnstring = appversion;

        }
        return returnstring;
    }

    public static String getagentname() {


        return agentname;
    }

    public static String getFaction() {


        return faction;
    }

    public static String getTrickster() {


        return trickster;
    }


}
