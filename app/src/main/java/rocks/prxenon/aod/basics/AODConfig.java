package rocks.prxenon.aod.basics;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import androidx.annotation.Nullable;

import static android.content.Context.MODE_PRIVATE;

public class AODConfig {
    public static FirebaseDatabase database;
    public static FirebaseUser user;
    public static com.google.firebase.messaging.FirebaseMessaging fmessaging = FirebaseMessaging.getInstance();
    public static SharedPreferences pref;
    public static DatabaseReference mDatabase, mDatabasemissions, mDatabaseaod, mDatabaserunnung, mdbroot;
    private static Context ctxx;

    public AODConfig() {


        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference("users");
        mDatabasemissions = database.getReference("missions");
        mdbroot = database.getReference("root");
        mDatabaseaod = database.getReference("aod");
        mDatabaserunnung = database.getReference("running");

        mDatabasemissions.keepSynced(true);
        mDatabaserunnung.keepSynced(true);
        mdbroot.keepSynced(true);

    }

    public AODConfig(Context cont) {

        ctxx = cont;

    }

    public AODConfig(String refferer, Context context) {

        ctxx = context;
        database = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        pref = context.getSharedPreferences("AppPref", MODE_PRIVATE);

        try {
            PackageInfo pInfo = ctxx.getPackageManager().getPackageInfo(ctxx.getPackageName(), 0);
            int verCode = pInfo.versionCode;
            new AgentData("appversion", verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public String getStringFromRawRes(int rawRes) {

        InputStream inputStream;
        try {
            inputStream = ctxx.getResources().openRawResource(rawRes);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            return null;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                inputStream.close();
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String resultString;
        try {
            resultString = byteArrayOutputStream.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        return resultString;
    }
}
