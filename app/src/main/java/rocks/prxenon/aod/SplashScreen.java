package rocks.prxenon.aod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.wang.avi.AVLoadingIndicatorView;

import androidx.annotation.NonNull;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        new AODConfig("splash", getApplicationContext());
        remoteconfig = FirebaseRemoteConfig.getInstance();


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                final AVLoadingIndicatorView avi = findViewById(R.id.avi);
                final TextView status_text = findViewById(R.id.status_txt);

                remoteconfig.fetch(0).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        remoteconfig.activateFetched();
                        avi.smoothToHide();
                        if (AgentData.getappversion("appversion") < remoteconfig.getLong("minversion")) {

                            avi.setVisibility(View.GONE);
                            status_text.setText("Deine Version von AOD ist veraltet.");
                            status_text.setVisibility(View.VISIBLE);
                        } else {
                            if (pref.getBoolean("userexist", false)) {
                                Intent i = new Intent(SplashScreen.this, FirebaseUIActivity.class);
                                i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            } else {
                                Intent i = new Intent(SplashScreen.this, IntroActivity.class);
                                startActivity(i);
                                finish();

                            }
                            // close this activity

                        }
                        Log.w("Appversion --> ", "app: " + AgentData.getappversion("appversion") + " remote: " + remoteconfig.getLong("minversion"));
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                if (pref.getBoolean("userexist", false)) {
                                    Intent i = new Intent(SplashScreen.this, FirebaseUIActivity.class);
                                    i.putExtra("login", true);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Intent i = new Intent(SplashScreen.this, IntroActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                                Log.w("Error --> ", "app: " + AgentData.getappversion("appversion") + " remote: " + remoteconfig.getLong("minversion") + " e: " + exception);
                                // Do whatever should be done on failure
                            }
                        });


            }
        }, SPLASH_TIME_OUT);
    }

}
