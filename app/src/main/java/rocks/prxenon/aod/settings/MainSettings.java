package rocks.prxenon.aod.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.pages.subs.LibsUsed;
import rocks.prxenon.aod.pages.subs.Privacy;

import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class MainSettings extends AppCompatPreferenceActivity {

    public static final int REQUEST_CODE = 892;
    private static PreferenceScreen mainprevview;
    private static Preference calibration, app_version, account_email, debug_text, trickster_status, used_libs, developer_name, privacy_terms;
    private static SharedPreferences prefs;
    private static ListPreference ingress_appart;
    private static CheckBoxPreference workaround;
    private static Context ctx;
    private static Activity atx;
    private static PreferenceGroup basics_settings;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        int index;

        @Override
        public boolean onPreferenceChange(Preference preference, final Object value) {
            Log.w("CHANGE -->", preference.getKey().toString() + " " + value.toString());
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                index = listPreference.findIndexOfValue(stringValue);

                if (preference.getKey().equalsIgnoreCase("ingress_appart")) {

                    Log.w("VALUE --->", Integer.parseInt(value.toString()) + " ." + value.toString());
                    if (Integer.parseInt(value.toString()) < 0) {
                        if (prefs.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(ctx, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle(R.string.settings_resetcal_q)
                                    .setMessage(R.string.settings_reset_m)
                                    .setPositiveButton(R.string.settings_oknext, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            calibration.setEnabled(false);
                                            calibration.setSummary(atx.getResources().getString(R.string.settings_choose_ingress));
                                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                                        }
                                    })
                                    .setNegativeButton(R.string.cancel_txt, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                            index = prefs.getInt("prever", -1);
                                        }
                                    })

                                    .show();
                        } else {

                            calibration.setEnabled(false);
                            prefs.edit().remove("calint").apply();
                            calibration.setSummary(atx.getResources().getString(R.string.settings_choose_ingress));
                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                        }

                    } else {
                        Log.w("Werte --->", "" + Integer.parseInt(value.toString()) + " " + prefs.getInt("prever", -1));
                        if (prefs.getBoolean(remoteconfig.getString("pref_calibration"), false) && Integer.parseInt(value.toString()) != prefs.getInt("prever", -1)) {
                            AlertDialog.Builder builder;
                            builder = new AlertDialog.Builder(ctx, android.R.style.Theme_Material_Dialog_Alert);
                            builder.setTitle(R.string.settings_resetcal_q)
                                    .setMessage(R.string.settings_reset_m)
                                    .setPositiveButton(R.string.settings_oknext, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            calibration.setSummary(R.string.settings_sum_notcal);
                                            prefs.edit().remove("calint").apply();
                                            prefs.edit().putInt("prever", Integer.parseInt(value.toString())).apply();
                                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                                        }
                                    })
                                    .setNegativeButton(R.string.cancel_txt, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                            prefs.edit().putInt("prever", prefs.getInt("prever", -1)).apply();
                                            index = prefs.getInt("prever", -1);
                                        }
                                    })

                                    .show();
                        } else {
                            if (prefs.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                calibration.setEnabled(true);
                                if (workaround.isChecked()) {
                                    calibration.setSummary(atx.getResources().getString(R.string.settings_debug_sum) +" Android 6.x");
                                } else {
                                    calibration.setSummary((atx.getResources().getString(R.string.settings_calibrated_for) +" " + listPreference.getEntries()[index]));
                                }
                            } else {
                                calibration.setEnabled(true);
                                prefs.edit().putInt("prever", Integer.parseInt(value.toString())).apply();
                                prefs.edit().remove("calint").apply();
                                prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                                calibration.setSummary(atx.getResources().getString(R.string.settings_sum_notcal));
                            }
                        }

                    }
                }

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                if (preference.getKey().equalsIgnoreCase("workaround")) {
                    if (stringValue.equalsIgnoreCase("true")) {
                        preference.setSummary(atx.getResources().getString(R.string.settings_workaround_man_sum));
                        basics_settings.addPreference(workaround);
                        basics_settings.removePreference(ingress_appart);
                        calibration.setEnabled(true);
                        calibration.setSummary("");
                        //prefs.edit().remove("calint").apply();
                        prefs.edit().remove("prever").apply();
                        prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                    } else {
                        if (prefs.getInt("calint", 0) == 0 && (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6" || Build.VERSION.RELEASE == "6.0.1" || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)) {
                            basics_settings.addPreference(workaround);
                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                            preference.setSummary(atx.getResources().getString(R.string.settings_forandroidsix));

                        }

                        basics_settings.addPreference(ingress_appart);
                    }
                } else {
                    preference.setSummary(stringValue);
                }
            }
            return true;
        }
    };
    private Typeface type, proto;
    private TextView mTitleTextView;

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static void bindPreferenceSummaryToValue2(CheckBoxPreference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        calibration = findPreference("ingress_callibration");
        app_version = findPreference("app_version");
        account_email = findPreference("account_email");
        debug_text = findPreference("debug_key");
        trickster_status = findPreference("trickster_status");
        used_libs = findPreference("libs_btn");
        privacy_terms = findPreference("privacy_terms");
        developer_name = findPreference("developer_name");

        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        ctx = getApplicationContext();
        atx = MainSettings.this;

        setupActionBar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.w("RESULT2 --->", requestCode + "");
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

                String requiredValue = data.getStringExtra("key");
                Log.w("RESULT --->", requiredValue);
                if (data.getIntExtra("claint", 0) != 0) {
                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                    prefs.edit().putInt("calint", data.getIntExtra("claint", 0)).apply();
                    calibration.setSummary(getResources().getString(R.string.settings_cal_suc));
                    debug_text.setSummary(prefs.getString("debugger", ""+R.string.settings_debug_sum));
                    int testconvert = 0;
                    String toconvert = data.getStringExtra("testvalue").replaceAll("\\s+", "");
                    toconvert = toconvert.replace('o', '0');
                    toconvert = toconvert.replace('O', '0');
                    toconvert = toconvert.replace('l', '1');
                    toconvert = toconvert.replace('i', '1');
                    toconvert = toconvert.replace('I', '1');
                    toconvert = toconvert.replace('L', '1');
                    toconvert = toconvert.replace(",", "");
                    toconvert = toconvert.replace(".", "");

                    try {
                        testconvert = Integer.parseInt(toconvert.replaceAll("[\\D]", ""));
                        Log.w("Test --->", "" + testconvert);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.settings_cal_suc_value)+ " " + testconvert, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            }
            if (requestCode == 654) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(this)) {
                        // startscreenshot();
                        Intent intent = new Intent(atx,
                                ScreenCalibration.class);
                        atx.startActivityForResult(intent, REQUEST_CODE);

                    }
                }
            }
        } catch (Exception ex) {
            Toast.makeText(MainSettings.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = (TextView) actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.settings_txt));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        startActivity(new Intent(MainSettings.this, AODMain.class));
        finish();
        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);

        //  super.onBackPressed();


    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            ctx = getContext();
            atx = getActivity();
            new AODConfig("init", ctx);

            prefs = getActivity().getSharedPreferences("AppPref", MODE_PRIVATE);
            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            debug_text = findPreference("debug_key");
            calibration = findPreference("ingress_callibration");
            account_email = findPreference("account_email");
            trickster_status = findPreference("trickster_status");
            trickster_status.setSummary(AgentData.getTrickster());
            privacy_terms = findPreference("privacy_terms");
            account_email.setSummary(AODConfig.user.getEmail().toString());
            used_libs = findPreference("libs_btn");
            developer_name = findPreference("developer_name");
            ingress_appart = (ListPreference) findPreference("ingress_appart");
            debug_text.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx, R.style.CustomDialog);

                    // builder.setMessage("Text not finished...");
                    LayoutInflater inflater = atx.getLayoutInflater();
                    View dialog = inflater.inflate(R.layout.dialog_setcalibration, null);
                    builder.setView(dialog);
                    final EditText input = (EditText) dialog.findViewById(R.id.inputcodetext);

                    input.setLines(1);
                    input.setHint(R.string.debug_text);
                    builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            prefs.edit().putInt("calint", Integer.parseInt(input.getText().toString())).apply();
                            debug_text.setSummary("manuell -> " + input.getText().toString());
                            prefs.edit().putString("debugger", getResources().getString(R.string.settings_cal_suc_value_man)+ " " + input.getText().toString()).apply();

                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                            // Todo publish the code to database
                        }
                    });

                    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            // Todo publish the code to database
                        }
                    });

                    builder.setCancelable(true);

                    if (!atx.isFinishing()) {
                        builder.create().show();

                    }

                    //open browser or intent here
                    return true;
                }
            });
            debug_text = findPreference("debug_key");
            debug_text.setSummary(prefs.getString("debugger", ""+getResources().getString(R.string.settings_debug_sum)));
            app_version = findPreference("app_version");
            app_version.setSummary("v" + AgentData.getappversion("appversion"));
            basics_settings = (PreferenceGroup) findPreference("basics_settings");
            privacy_terms.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent ix = new Intent(ctx, Privacy.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);

                    //open browser or intent here
                    return true;
                }
            });
            used_libs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent ix = new Intent(ctx, LibsUsed.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);

                    //open browser or intent here
                    return true;
                }
            });
            developer_name.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    String url = "https://www.paypal.me/PrXenon";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                    //open browser or intent here
                    return true;
                }
            });
            trickster_status.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                    atx.startActivity(intent);

                    //open browser or intent here
                    return true;
                }
            });

            calibration.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    if (workaround.isChecked()) {
                        Intent intent = new Intent(getActivity(),
                                ScreenCalibrationWorkaround.class);
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    } else {
                        if (!Settings.canDrawOverlays(atx)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + atx.getPackageName()));
                            atx.startActivityForResult(intent, 654);
                        } else {
                            Intent intent = new Intent(getActivity(),
                                    ScreenCalibration.class);
                            getActivity().startActivityForResult(intent, REQUEST_CODE);
                        }
                    }

                    //open browser or intent here
                    return true;
                }
            });

            workaround = (CheckBoxPreference) findPreference("workaround");
            mainprevview = getPreferenceScreen();
            if (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6" || Build.VERSION.RELEASE == "6.0.1" || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                ((PreferenceGroup) findPreference("basics_settings")).addPreference(workaround);
            } else {
                ((PreferenceGroup) findPreference("basics_settings")).removePreference(workaround);
                // ((PreferenceGroup) findPreference("basics_settings")).addPreference(workaround);

            }
            bindPreferenceSummaryToValue2(workaround);
            if (workaround.isChecked()) {

            } else {
                bindPreferenceSummaryToValue(findPreference("ingress_appart"));
            }


        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), AODMain.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }


}
