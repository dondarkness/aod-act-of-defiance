package rocks.prxenon.aod.helper;

import android.content.Intent;

public interface ServiceCallBack {

    void connectToDevice(Intent intent);

    void closewindow();
}
