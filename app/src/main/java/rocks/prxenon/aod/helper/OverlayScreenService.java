package rocks.prxenon.aod.helper;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;

import java.util.List;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AgentData;


public class OverlayScreenService extends Service implements View.OnClickListener {

    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    private ServiceCallBack serviceCallBack;
    private WindowManager windowManager;
    private WindowManager windowManager2;
    // private ImageView chatHead;
    private LinearLayout open, rootv;
    private Button back;
    private Bundle bundle;
    private TextView details;
    private String comp, basistext, agentname, clientis, themid, theartid;
    private Intent getint;
    private LinearLayout rootContent;
    private Activity act;
    private ContentResolver cres;
    private ContentObserver obs;

    public void setServiceCallBack(Activity aodMain) {
        act = aodMain;
    }


    public void setCallbacks(ServiceCallBack callbacks) {
        serviceCallBack = callbacks;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * method for clients
     */
    public int getRandomNumber() {
        return mGenerator.nextInt(100);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);


        bundle = intent.getExtras();


        if (bundle != null) {

            comp = bundle.getString("comp");
            basistext = bundle.getString("basistext");
            agentname = bundle.getString("agentname");
            clientis = bundle.getString("ingress");
            themid = bundle.getString("mid");
            theartid = bundle.getString("aid");


        }
        initial();
        return START_STICKY;
    }

    public void startIngress() {

        // details.setText(Html.fromHtml("AGENT <b>" + agentname + "</b><br><b>1.</b> Öffne dein Agent Profil<br><b>2.</b> Klicke auf die Badge  <b>" + basistext.replaceAll(" ","-") + "</b><br><b>3.</b> Es muss zeigen <b>" + comp + "</b><br>4. Erstelle <b>manuell</b> einen Screenshot."));
        //ingress.setVisibility(View.GONE);

        PackageManager manager = getApplicationContext().getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(clientis);
            if (i == null) {
                //return false;
                throw new PackageManager.NameNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            getApplicationContext().startActivity(i);


        } catch (PackageManager.NameNotFoundException e) {

        }
        // Toast.makeText(this, "Ingress wird gestartet", Toast.LENGTH_SHORT).show();
    }

    private void connect(String tmp, String mart, String actionid) {


        String address = tmp;

        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        intent.putExtra("screen", address);
        intent.putExtra("missionart", mart);
        intent.putExtra("onactionid", actionid);

        //onactionid, missionart);
        Log.w("CONNECT -->", address);

        if (serviceCallBack != null) {
            serviceCallBack.connectToDevice(intent);
        }


        ActivityManager am2 = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = am2.getAppTasks();
        Log.w("Back -->", "pressed " + tasks.size());
        for (ActivityManager.AppTask task : tasks) {
            if (task.getTaskInfo().baseActivity.getPackageName().contains("rocks.prxenon.aod")) {
                Log.i("Point", task.getTaskInfo().baseIntent.getComponent().getPackageName() + " api 21");
                task.moveToFront();
                Intent svc = new Intent(getApplicationContext(), OverlayScreenService.class);
            }

        }


        cres.unregisterContentObserver(obs);

    }

    public void initial() {
        cres = getContentResolver();

        obs =
                (new ContentObserver(new Handler()) {
                    @Override
                    public boolean deliverSelfNotifications() {
                        Log.d("hai", "deliverSelfNotifications");
                        return super.deliverSelfNotifications();
                    }

                    @Override
                    public void onChange(boolean selfChange) {
                        super.onChange(selfChange);
                    }

                    @Override
                    public void onChange(boolean selfChange, Uri uri) {
                        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(act);
                        boolean trickster = true;
                        String currentApp;
                        //Log.i("Trickster Status--->", AgentData.getTrickster());
                        if (AgentData.getTrickster().equalsIgnoreCase("green")) {
                            trickster = false;
                        }

                        if (trickster) {
                            if (!isAccessGranted()) {
                                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                                act.startActivity(intent);
                            } else {
                                UsageStatsManager usm = (UsageStatsManager) act.getSystemService(Context.USAGE_STATS_SERVICE);
                                long time = System.currentTimeMillis();
                                List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 10000 * 10000, time);
                                if (appList != null && appList.size() == 0) {
                                    Log.d("Executed app", "######### NO APP FOUND ##########");
                                }
                                if (appList != null && appList.size() > 0) {
                                    SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                                    for (UsageStats usageStats : appList) {

                                        //  Log.d("Executed app", "usage stats executed : " + usageStats.getPackageName() + "\t\t ID: ");
                                        mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                                    }
                                    if (mySortedMap != null && !mySortedMap.isEmpty()) {

                                        currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();

                                        if (currentApp.equalsIgnoreCase("com.nianticproject.ingress") || currentApp.equalsIgnoreCase("android") || currentApp.equalsIgnoreCase("com.nianticlabs.ingress.prime.qa")) {
                                            trickster = false;
                                        }
                                        Log.d("currentApplast", "usage stats executed : " + currentApp + "\t\t ID: ");


                                    }
                                }
                            }
                        }

                        if (trickster) {
                            //connect(path, theartid, themid);
                            StyleableToast.makeText(getApplicationContext(), "Bitte Ingress verwenden ", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        } else {
                            // Log.i("App----> ", activityName);
                            if (uri.toString().matches(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString() + "/[0-9]+") || uri.toString().matches(MediaStore.Files.getContentUri("external").toString() + "/[0-9]+")) {


                                try (Cursor cursor = getContentResolver().query(uri, new String[]{
                                        MediaStore.Images.Media.DISPLAY_NAME,
                                        MediaStore.Images.Media.DATA
                                }, null, null, null)) {
                                    if (cursor != null && cursor.moveToFirst()) {
                                        final String fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
                                        final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                                        // TODO: apply filter on the file name to ensure it's screen shot event
                                        Log.d("file", "FILE CHANGE OCCURED " + fileName + " " + path);


                                        connect(path, theartid, themid);

                                    }
                                }
                            }
                        }
                        super.onChange(selfChange, uri);
                    }
                }
                );

        if (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6") {
            cres.registerContentObserver(
                    MediaStore.Files.getContentUri("external"),
                    true, obs);
        } else {
            cres.registerContentObserver(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    true, obs);

        }

    }

    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                    applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        // chatHead = new ImageView(this);

        // chatHead.setImageResource(R.drawable.floating);

        open = (LinearLayout) LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.calibrationscreen, null);
        back = (Button) open.findViewById(R.id.gobackb);
        // ingress = (Button) open.findViewById(R.id.ingress);

        //details = (TextView) open.findViewById(R.id.textdescr);


        // details.setText(basistext+" "+comp);
        back.setOnClickListener(this);
        // ingress.setOnClickListener(this);
        final WindowManager.LayoutParams params;
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
            params.x = 200;
            params.y = 200;


            // windowManager.addView(chatHead, params);
            windowManager.addView(open, params);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
            params.x = 200;
            params.y = 200;
            windowManager.addView(open, params);
        }

       /* try {
            open.setOnTouchListener(new View.OnTouchListener() {
                private WindowManager.LayoutParams paramsF = params;

                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            // Get current time in nano seconds.

                            initialX = paramsF.x;
                            initialY = paramsF.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            paramsF.x = initialX + (int) (event.getRawX() - initialTouchX);
                            paramsF.y = initialY + (int) (event.getRawY() - initialTouchY);

                            windowManager.updateViewLayout(open, paramsF);

                            break;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (open != null) windowManager.removeView(open);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.gobackb:

                if (serviceCallBack != null) {
                    serviceCallBack.closewindow();
                }

                cres.unregisterContentObserver(obs);


                //Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
                break;


        }


    }

    public class LocalBinder extends Binder {
        public OverlayScreenService getService() {
            // Return this instance of LocalService so clients can call public methods
            return OverlayScreenService.this;
        }
    }


}